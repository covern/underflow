How to handle function calls.

For how to do function calls and why Underflow's approach is sound
see the accompanying Isabelle file: UnderApproxProcCalls.thy

# Symbolic Execution for Function Calls

This suggests that (in the absence of global variables)
like with the rules for if/while, we should
symbolically execute at a function call by consuming the presumption,
causing spatial assertions to be matched and implicitly moving all pure
assertions in the state not implied by the presumption into the frame.
Of course along the way we can infer and back-propagate
extra assertions as needed to ensure consumption succeeds (unless it can't
e.g. because we have p |-\-> in the state but need to match p |--> v).

# Does this make sense? An example.

`func2` below definitely satisfies its under-approximate contract:
for all "final" states (in which x refers to x's initial value, remember)
in which "x >= 0" (i.e. x was >= 0 initially) and result == x + 1, there
exists an initial state satisfying `x >= 0` that can reach the final one.
In fact this seems to be the /strongest/ triple we could write down above
that particular code parth in the function.

```
int func2(int x)
  _(presumes x >= 0 covers  x >= 0 && result == x+1)
{
  if(x >= 0){
    int y = x;
    x = 5;
    return (y+1);
  }else{
    return x;
  }
}
```

Now consider `func1`. When it calls `func2` we know that
`x > 0`.

```
void func1(int x){
  if(x > 0) return func2(x);
}
```
However the function call rule proved in Isabelle suggests that we:
First consume the presumption. Doing so 
enriches our state to become `x >= 0 && x > 0` (which is of course
fine, justified by consequence rule). Then let the frame F
be `x > 0`. The presumption then implies the remainder, so we can then
produce the result
to derive that `func1` can return `x+1` in this case, as one should expect.

# No Need for Back-Propagating Non-Spatial Assertions ?

Of course we could have made this work regardless of the presumption
on `func2`, i.e. it would have worked equally well with `true` as its
presumption.

This suggests that in general there is no need to back-propagate
non-spatial assertions that refer only to the functions arguments
or its result, as we will surely pick them
up in the covers anyway.

Indeed for any triple
```
[ P(v1...vn) ] func(v1...vn) [ Q(v1...vn,result) ]  
```
if we factor out the non-spatial part of `Q` that refers to `v1`...`vn`, calling it
`Q'(v1...vn)`, then if the above triple holds then so does
```
[ P(v1...vn) * Q'(v1...vn) ] func(v1...vn) [ Q(v1...vn,result) ]
```
by the fact that the store in which `Q` is evaluated refers to the initial values of
`v1`...`vn`. (We prove this in Isabelle too.)

So, back-propagating non-spatial assertions that refer only to `v1`...`vn`.
doesn't seem to give a stronger statement
(even in principle), and the weaker statement seems just as useful e.g. for test
generation since we can extract the stronger one from it.

What about non-spatial assertions that refer to logical variables that represent
values in the heap? Well those logical variables are also "external variables"
of the function, and won't change over the lifetime of the function. They are
essentially like function arguments that appear only in the specification.
So those don't need to be back-propagated either.

Consider the following example and notice that the assertion v :: low in the
result of course implies v :: low in the presumption too:
```
void load_and_output(int *p)
  _(presumes exists int v. p |-> v covers p |-> 0 && v :: low)
{
  int x = *p;
  output_low(x);
  *p = 0;
}
```
where `output_low(x)`'s contract says it fails when `x :!: low` and
succeeds when `x :: low`. Both of these contracts are satisfied here.
However the second one is strictly more informative than the first and
cannot be derived from the first one alone.

It seems like the only time we might need to back-propagate is for
assertions that refer to global variables. Noting that the above
rule proved for function calls would not aply in that case anyway.
But even if we could handle globals, there would still be no need
to back-propagate pure assertions because we can capture the
value of the global variable by a logical variable (much like
in the heap example above). 

# Take-Aways:

1. When calling a function, we can conjoin non-spatial assertions to
   the state. 

2. When proving that a `covers` is satisfied, we still need to prove that
   the `covers` implies the current path condition.

3. When deciding whether to back-propagate a newly conjoined non-spatial
   assertion, we don't ever need to do that.

# Still Need to Prove Implication for `covers` satisfaction

Supose we modified `func2` above as follows, producing `func3`.
This contract is not satisfied: there exists final states that
satisfy the `covers` (in which `x == 0 && result == 1`) that
cannot be reached from any initial state satisfying `x > 0`.


```
int func3(int x)
  _(presumes x > 0 covers x >= 0 && (result == x+1)
{
  if(x > 0){
    return (x+1);
  }else{
    return x;
  }
}
```

