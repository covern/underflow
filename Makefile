.PHONY: all test clean mrproper parser check-dependencies

MILL = ./mill

UNDERFLOW_EXAMPLES = examples

VPATH     	= $(UNDERFLOW_EXAMPLES)
UNDERFLOW_C    	= $(wildcard $(UNDERFLOW_EXAMPLES)/*.c)
UNDERFLOW_OBJ  	= $(UNDERFLOW_C:$(UNDERFLOW_EXAMPLES)/%.c=obj/%.o)
UNDERFLOW_JAVA 	= underflow/src/underflow/c/Parser.java \
              underflow/src/underflow/c/Scanner.java
UNDERFLOW_SCALA	= $(shell find underflow/src -iname "*.scala")

CC ?= cc
CFLAGS ?= -Iinclude -Wall -W -Wpedantic

BEAVER = ./beaver
JFLEX  = ./jflex

UNDERFLOW_JAR = out/underflow/jar/dest/out.jar

UNDERFLOW_LAUNCHER = out/underflow/launcher/dest/run
UNDERFLOW_SH  = ./Underflow.sh

all: $(UNDERFLOW_JAR) $(UNDERFLOW_SH)

parser: $(UNDERFLOW_JAVA)

test:
	$(MILL) underflow.test

clean:
	$(MILL) clean
	rm -f $(UNDERFLOW_SH)

mrproper: clean
	rm -f $(UNDERFLOW_JAVA)

check-dependencies:
	$(MILL) mill.scalalib.Dependency/updates

$(UNDERFLOW_LAUNCHER):
	@echo $@
	$(MILL) underflow.launcher

$(UNDERFLOW_JAR): $(UNDERFLOW_JAVA) $(UNDERFLOW_SCALA)
	@echo $@
	$(MILL) underflow.jar

$(UNDERFLOW_SH): $(UNDERFLOW_LAUNCHER)
	@echo "[echo]  $@"; echo "#!/usr/bin/env bash" > $@; echo "export LD_LIBRARY_PATH=$(PWD)/underflow/lib" >> $@; echo "source $(PWD)/$(UNDERFLOW_LAUNCHER)" >> $@
	@echo "[chmod] $@"; chmod +x $@

%.java: %.grammar
	$(BEAVER) -t $^

%.java: %.flex
	$(JFLEX) -nobak $^

o: $(UNDERFLOW_OBJ)
	@echo $(UNDERFLOW_OBJ)

