# Underflow

A prototype program analysis tool for discovering and
proving information flow security violations of C code.
Underflow automates Insecurity Separation Logic (InsecSL)
reasoning via symbolic execution.

This tool was built by modifying the SecC tool
(https://bitbucket.org/covern/secc). 

## Requirements

* Java 8

## Supported Platforms

* Linux 
* Mac OS 

## Installation

### Java 8

First check that Java 8 is installed. Run `java -version` and look for
a version number that begins with `1.8` or above.

## Building Underflow

Underflow should build by simply typing `make` in the top-level directory.

```
make
```

This should produce a shell script `Underflow.sh` for running Underflow.

## Running Underflow

Fuller documentation is contained in the file `examples/insec.c` via
comments at the top of that file.

By default, Underflow runs in "validate" mode in which it attempts to validate
function contracts in Insecurity Separation Logic. 

Simply run `Underflow.sh`, supplying a list of files to analyse as command line
arguments.

```
./Underflow.sh file1.c file2.c ...
```

The bundled file `examples/insec.c` lists a number of contracts for small
example programs, as positive test cases.

The bundled file `examples/unreachable.c` lists a number of false contracts
that do not hold, as negative test cases.

In Underflow's "discover" mode, it attempts to discover contracts for each
function and to use those contracts to infer contracts for other functions.

This mode is enabled using the `-discover` command-line option.

```
./Underflow.sh -discover file1.c file2.c ...
```

The bundled case study `examples/auction.c` contains a leakage vulnerability
that Underflow discovers. Detecting this vulnerability requires an
inter-procedural analysis, which Underflow carries out, as it arises from the
interaction of multiple functions. The vulnerability is reported via the
"insecure" contract inferred for the top-level function `run_auction()`.

Other case studies are also present in the examples/ directory as test
cases for contract discovery.

These include the infamous Lucky13 vulnerability and its patch as well as many
other cryptographic-related functions adapted from the Binsec/Rel benchmarks
(https://github.com/binsec/rel_bench).





