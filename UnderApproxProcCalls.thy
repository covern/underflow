theory UnderApproxProcCalls
  imports Main
begin

typedecl val
typedecl var
type_synonym store = "var => val"
type_synonym heap = "val => val option option"

type_synonym proc = "store \<Rightarrow> heap \<Rightarrow> store \<Rightarrow> heap \<Rightarrow> bool"

type_synonym pred = "store \<Rightarrow> heap \<Rightarrow> bool"
type_synonym pure = "store \<Rightarrow> bool"

definition getPure :: "pred \<Rightarrow> pure" where
  "getPure P \<equiv> \<lambda>s. \<exists>h. P s h"

definition getSpatial :: "pred \<Rightarrow> pred" where
  "getSpatial P \<equiv> \<lambda>s h. getPure P s \<longrightarrow> P s h"

lemma gets_work:
  "(getPure P s \<and> getSpatial P s h) = P s h"
  by(auto simp: getPure_def getSpatial_def)

definition triple :: "pred \<Rightarrow> proc \<Rightarrow> pred \<Rightarrow> bool" where
  "triple P c Q \<equiv> (\<forall>s h. Q s h \<longrightarrow> (\<exists>s0 h0. P s0 h0 \<and> c s0 h0 s h))"

definition local :: "proc \<Rightarrow> bool" where
  "local c \<equiv> \<forall>s0 h0 s h. c s0 h0 s h \<longrightarrow> s0 = s"


definition implies :: "('a \<Rightarrow> bool) \<Rightarrow> ('a \<Rightarrow> bool) \<Rightarrow> bool" where
  "implies Q P \<equiv> \<forall>s. Q s \<longrightarrow> P s"

text {*
  This lemma says that in the absence of global variables there is never
  any point back-propagating pure assertions.
*}
lemma pure_presumption_is_never_stronger_than_pure_result:
  "triple P c Q \<Longrightarrow> local c \<Longrightarrow> implies (getPure Q) (getPure P)"
  by(fastforce simp: triple_def local_def implies_def getPure_def)

lemma meh1:
  "(\<forall>s h. P s h \<longrightarrow> getPure Q s) \<Longrightarrow>
   implies (getPure P) (getPure Q)"
  by(auto simp: getPure_def implies_def)

definition sep :: "heap \<Rightarrow> heap \<Rightarrow> heap \<Rightarrow> bool"where 
  "sep h h1 h2 \<equiv> h1 ++ h2 = h \<and> dom h1 \<inter> dom h2 = {}"

definition star :: "pred \<Rightarrow> pred \<Rightarrow> pred" where
  "star P Q \<equiv> (\<lambda>s h. \<exists>h1 h2. sep h h1 h2 \<and>
                             P s h1 \<and> Q s h2)"

axiomatization where
  frame_rule: "local c \<Longrightarrow> triple P c Q \<Longrightarrow> triple (star P F) c (star Q F)"
  
text {*
  This is the high level rule for function calls. While the conseq
  obligation looks backwards, it is not a problem. We put the pure 
  part of of R into the frame F. Then we just need to match up the
  spatial parts of P and R, noting that we can have stuff left-over
  in R after matching: this is exactly what consumption of P in R
  does. Note that R here we should think of as the current result
  after we have inferred and back-propagated missing heap chunks 
  needed to satisfy P.

  Thus the current approach implemented in InsecC is sound:
  1. Consume the presumption P, thereby ensuring all spatial
     parts are matched against R and dropped, leaving just the
     frame F behind. During consumption we can infer additional
     assertions and back-propagate as needed, as usual.
  2. Then produce the result Q, leaving Q * F as the new state.
  
  This also explains why this doesn't work so easily for globals: the 
  frame rule relies on any variables modified by c not being mentioned
  in the frame. Which means we can't move any pure assertions about
  global variables from R into F, since we can't frame over them.
  Instead we would have to prove that they follow from the (pure)
  assertions in P.
*}
lemma proccalls:
  assumes loc:"local c"
  assumes trip: "triple P c Q"
  assumes conseq: "\<forall>s h. star P F s h \<longrightarrow> R s h"
  shows "triple R c (star Q F)"
  using assms frame_rule triple_def
  by metis

definition uheap :: "pred \<Rightarrow> bool" where
  "uheap P \<equiv> \<forall>s h1 h2. P s h1 \<longrightarrow> P s h2 \<longrightarrow> h1 = h2"

lemma getSpatial_imp: "P s h \<Longrightarrow> (getSpatial P) s h"
  by(auto simp: getSpatial_def)

lemma testing_lemma:
  assumes loc:"local c"
  assumes trip: "triple P c Q"
  assumes pure: "getPure Q s"
  assumes spatial: "getSpatial P s h0"
  assumes fin: "Q s h"
  assumes uheap: "uheap (getSpatial P)"
  shows "c s h0 s h"
proof -
  from fin trip obtain s0w h0w where
    "P s0w h0w" "c s0w h0w s h" unfolding triple_def by blast
  with loc have [simp]: "s0w = s" unfolding local_def by auto
  have "P s h0" 
    using \<open>P s0w h0w\<close> \<open>s0w = s\<close> gets_work spatial by blast
  with `P s0w h0w` `uheap (getSpatial P)` have [simp]: "h0w = h0" unfolding uheap_def using getSpatial_imp by fastforce
  thus "c s h0 s h" using `c s0w h0w s h` by simp
qed
  
definition pto :: "var \<Rightarrow> var \<Rightarrow> pred" where
  "pto p v \<equiv> \<lambda>s h. dom h = {(s p)} \<and> h (s p) = Some (Some (s v))"

lemma pto_uheap:
  "uheap (pto p v)"
  apply(clarsimp simp: uheap_def pto_def)
  by (metis dom_eq_singleton_conv fun_upd_triv fun_upd_upd)

definition pinvalid :: "var \<Rightarrow> pred" where
  "pinvalid p \<equiv> \<lambda>s h. dom h = {(s p)} \<and> h (s p) = Some None"

lemma pinvalid_uheap:
  "uheap (pinvalid p)"
  apply(clarsimp simp: uheap_def pinvalid_def)
  by (metis dom_eq_singleton_conv fun_upd_triv fun_upd_upd)

lemma sep_uheap:
  "sep h h1 h2 \<Longrightarrow> sep h' h1 h2 \<Longrightarrow> h = h'"
  by(clarsimp simp: sep_def)

lemma star_uheap:
  "uheap P \<Longrightarrow> uheap Q \<Longrightarrow> uheap (star P Q)"
  using sep_uheap unfolding star_def 
  by (smt uheap_def)


text {* 
  The above is sufficient to justify testing. In particular, remember
  that existentials in the presumption in InsecC are really universal
  quantification over free variables that also appear in the result, where
  that universal quantification happens outside the separation logic
  assertion (i.e. at the meta level). This can be seen by looking at the
  example symex formalised in Isabelle and the formulas generated by the
  rules. Hence no need to consider exists here.
*}

end

