package underflow

import underflow.pure.Backend

object Underflow {
  var dry: Boolean = false

  def configure(args: List[String]): List[String] = args match {
    case Nil => Nil

    case "-debug:axioms" :: rest =>
      c.log.info("Built-In Axioms")
      c.log.shift {
        for (ax <- pure.axioms) {
          c.log.info(ax)
        }
      }
      configure(rest)

    case "-debug:solver" :: rest =>
      Backend.debug = true
      configure(rest)

    case "-debug" :: rest =>
      c.log.level = c.log.Debug
      configure(rest)

    case "-dry" :: rest =>
      dry = true
      configure(rest)

    case "-no:div" :: rest =>
      import underflow.pure.Fun
      import underflow.pure.Solver
      Solver.uninterpreted += Fun.divBy
      configure(rest)

    case "-no:mod" :: rest =>
      import underflow.pure.Fun
      import underflow.pure.Solver
      Solver.uninterpreted += Fun.mod
      configure(rest)

    case "-timeout" :: s :: rest =>
      import underflow.pure.Solver
      Solver.timeout = s.toInt
      configure(rest)

    case "-timeout" :: _ =>
      throw new UnsupportedOperationException("-timeout requires an argument")

    /* ignore insecurity (information leakage) */
    case "-ignore:insecurity" :: rest =>
      import underflow.c.Exec
      Exec.report_insecurity = false
      configure(rest)

    /* ignore memory safety errors */
    case "-ignore:errors" :: rest =>
      import underflow.c.Exec
      Exec.report_errors = false
      configure(rest)

    case "-loop" :: n :: rest =>
      import underflow.c.Exec
      Exec.loop = n.toInt
      configure(rest)

    case "-discover" :: rest =>
      c.log.analysisMode = c.AnalysisMode.discover
      configure(rest)

    case "-strengthen" :: rest =>
      c.log.strengthen = true
      configure(rest)

    case file :: rest =>
      file :: configure(rest)
  }

  def main(args: Array[String]) {

    if (args.isEmpty) {
      println("usage: underflow [-discover] [-loop N] [-timeout Nmillisecs] [-ignore:errors] [-ignore:insecurity] [-strengthen] [-debug] [-debug:axioms] [debug:solver] file1.c file2.c ...")
    } else {
      val files = configure(args.toList)
      for (file <- files) {
        try {
          c.log.info(file)
          if (dry)
            c.parse(file)
          else
            c.verify(file)
        } catch {
          case e: java.io.FileNotFoundException =>
            Console.err.println("  file does not exist: " + file)
          case error.InvalidProgram(msg @ _*) =>
            Console.err.println("  invalid program: " + msg.mkString(" "))
          case _: error.Error =>
          // ignored on the command line
        }
      }
    }
  }
}