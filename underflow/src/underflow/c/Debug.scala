package underflow.c

import underflow.pure.Pure
import underflow.error

import underflow.pure.Var
import underflow.heap.Prop
import underflow.pure.Subst
import scala.util.DynamicVariable

sealed trait AnalysisMode

object AnalysisMode {
  case object discover extends AnalysisMode
  case object validate extends AnalysisMode
}

sealed trait Trace

import underflow.pure

object trace {
  case class VerificationFailure(trace: List[(Stmt, State)], info: Any*) extends error.Error
  def debug(tr: List[(Stmt, State)]): Unit = tr match {
    case Nil =>
    case (stmt, st) :: rest =>
      log.debug(stmt)
      debug(rest)
  }
  
  def within[A](aux: Aux, st: State)(f: State => A): A = {
    within(Ghost(aux), st)(f)
  }

  // XXX: consider moving these to State.scala
  def backprop(q: Prop, tr: List[(Stmt, State)]): List[(Stmt, State)] = tr match {
    case Nil => Nil
    case (stmt,st) :: rest => {
      val pt = q // XXX: no need to transform, see 'pre' in State.scala
      val restt: List[(Stmt, State)] = backprop(pt,rest)
      val st2s = st.&&(pt)
      val st2 = st2s match {
        case Nil => st
        case hd :: rest => hd
      }
      ((stmt,st2) :: restt)
    }
  }

  def backprop(q: Pure, tr: List[(Stmt, State)]): List[(Stmt, State)] = tr match {
    case Nil => Nil
    case (stmt,st) :: rest => {
      val pt = q // XXX: no need to transform, see 'pre' in State.scala
      val restt: List[(Stmt, State)] = backprop(pt,rest)
      val st2s = st.&&(pt)
      val st2 = st2s match {
        case Nil => st
        case hd :: rest => hd
      }
      ((stmt,st2) :: restt)
    }
  }

  def within[A](stmt: Stmt, st: State)(f: State => A): A = {
    val st2 = st.extendTrace(stmt, st)
    try {
      val r = f(st2);
      r
    } catch {
      case e: Throwable =>
        log.debug("error trace")
        log.shift {
          debug(st2.tr)
        }
        e match {
          case error.VerificationFailure(info @ _*) =>
            throw trace.VerificationFailure(st.tr, info: _*)
          case pure.ProofUnknown(info @ _*) =>
            throw trace.VerificationFailure(st.tr, info: _*)
          case _ =>
            throw e
        }
    }
  }
}

object log extends DynamicVariable(0) {
  val Debug = 3
  val Info = 2
  val Error = 1
  val Quiet = 0

  var level = Info

  var strengthen = false
  var analysisMode: AnalysisMode = AnalysisMode.validate
  
  def indent = "  " * value

  def print(how: Int, nl: Boolean, args: Any*) {
    val text = args.mkString(indent, " ", "")
    if (level >= how) {
      if (nl) Console.println(text)
      else Console.print(text)
    }
  }

  def debug_(args: Any*) {
    print(Debug, false, args: _*)
  }

  def info_(args: Any*) {
    print(Info, false, args: _*)
  }

  def error_(args: Any*) {
    print(Error, false, args: _*)
  }

  def debug(args: Any*) {
    print(Debug, true, args: _*)
  }

  def info(args: Any*) {
    print(Info, true, args: _*)
  }

  def error(args: Any*) {
    print(Error, true, args: _*)
  }

  def shift[A](f: => A): A = {
    withValue(value + 1)(f)
  }
}