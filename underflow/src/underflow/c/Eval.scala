package underflow.c

import java.math.BigInteger

import underflow.pure.Param
import underflow.pure.Pure
import underflow.pure.Const
import underflow.error
import underflow.pure.Sort
import underflow.pure.Fun
import underflow.pure.Rewrite
import underflow.pure.Var
import underflow.pure.Ex
import underflow.pure.All
import underflow.pure.Simplify

object Eval {
  import Prove.prove
  import Prove.consume
  import Prove.produce

  def rvals(exprs: List[Expr], st0: State, ctx: Context): List[(Status, List[Pure], State)] = exprs match {
    case Nil =>
      List((Status.ok, Nil, st0))

    case expr :: rest => // XXX: right-to-left, should be parallel
      for (
        (k1, xs, st1) <- rvals(rest, st0, ctx);
        (k2, x, st2) <- rval(expr, st1, ctx)
      ) yield (Status.bind(k1,k2), x :: xs, st2)
  }

  val ZeroConst = Const.int(0)
  val OneConst = Const.int(1)

  // truth and int_of_bool we hope act as inverses of each other
  def truth(arg: Pure): Pure = arg.typ match {
    case Sort.bool => arg
    case Sort.word(_,_) => arg match {
      case Pure.ite(a,OneConst,ZeroConst) => a
      case _ => arg !== Pure.coerce(ZeroConst,arg.typ)
    }
    case Sort.pointer(_) => arg !== Fun._null()
    case _ => throw error.InvalidProgram("not boolean", arg)
  }

  def int_of_bool(arg: Pure): Pure = arg.typ match {
    case Sort.int => arg
    case Sort.bool => arg match {
      case Pure.not(Pure._eq(v,ZeroConst)) => v
      case _ =>
       (Pure.ite(arg,OneConst,ZeroConst))
    }
    case _ => throw error.InvalidProgram("not boolean", arg)
  }

  def app(fun: Fun, args: Pure*) = {
    val _args = for ((typ, arg) <- fun.args zip args) yield typ match {
      case Sort.bool => truth(arg)
      case _ => arg
    }
    val res = fun(_args: _*)
    if (!res.isRelational) {
      fun.ret match {
        case Sort.bool => {
          int_of_bool(res)
        }
        case _ => res
      }
    } else {
      res
    }
  }

  def add(arg1: Pure, arg2: Pure, ctx: Context) = (arg1.typ, arg2.typ) match {
    // XXX: currently we coerce all index types to int
    case (Sort.pointer(elem), _) => ctx index (arg1, Pure.coerce(arg2,Sort.int))
    case (a, b) => arg1 + Pure.coerce(arg2,arg1.typ)
    case _ => throw error.InvalidProgram("invalid arithmetic operation", arg1 + " + " + arg2, arg1.typ, arg2.typ)
  }

  def sub(arg1: Pure, arg2: Pure, ctx: Context) = (arg1.typ, arg2.typ) match {
    case (Sort.pointer(elem), ctx.int) => ctx index (arg1, -arg2)
    case (a, b) => arg1 - Pure.coerce(arg2,arg1.typ)
    case _ => throw error.InvalidProgram("invalid arithmetic operation", arg1 + " - " + arg2, arg1.typ, arg2.typ)
  }

  def lower(arg1: Pure, arg2: Pure, ctx: Context) = (arg1.typ, arg2.typ) match {
    case (Sort.sec, Sort.sec) => arg1 lower arg2
    case (a, b) => int_of_bool(arg1 <= Pure.coerce(arg2,arg1.typ)) // TODO: other numeric types
    case _ => throw error.InvalidProgram("invalid comparison operation", arg1 + " <= " + arg2, arg1.typ, arg2.typ)
  }

  def rval_test(expr: Expr, st0: State, ctx: Context): List[(Status, Pure, State)] = {
    for ((k, _res, st1) <- rval(expr, st0, ctx)) yield {
      (k, truth(_res), st1)
    }
  }

  /* def rval_low_test(expr: Expr, st0: State, ctx: Context): List[(Pure, State)] = {
    for ((_res, st1) <- rval(expr, st0, ctx)) yield {
      val _test = truth(_res)
      assert(!_test.isRelational, "relational assertion as rval")
      // remove security check and replace by satisfiability of insecurity
      //prove(_test :: st1.attacker, st1, ctx)
      // check for satisfiability of insecurity

      val insec = (_test :!!: st1.attacker)
      (st1 && insec) match {
        case Nil =>
          (_test, st1)
        case st2 :: rest => {
          st2.stronglyConsistent(Some(ctx)) match {
            case Nil =>
              (_test, st1)
            case stc :: rest =>
              throw error.VerificationFailure("insecure", insec, "is satisfiable", stc)
          }
        }
      }
    }
  } */


  def asg(lhs: Expr, rhs: Expr, st0: State, ctx: Context): List[(Status, Pure, Pure, State)] = lhs match {
    case id: Id if (st0.store contains id) =>
      ctx.checkWrite(id)
      val _old = st0.store(id)
      for (
        (k, _rhs, st1) <- rval(rhs, st0, ctx)
      ) yield {
          (k, _old, _rhs, st1 assign (id, _rhs, ctx)) 
      }

    case Index(base, index) =>
      val expr = PreOp("*", BinOp("+", base, index))
      asg(expr, rhs, st0, ctx)

    case PreOp("*", ptr) =>
      ctx.checkStore()
      for (
        (k1, _rhs, st1) <- rval(rhs, st0, ctx);
        (k2, _ptr, st2) <- rval(ptr, st1, ctx);
        (k3, _old, st3) <- st2 store (_ptr, _rhs)
      ) yield {
        (Status.bind(k1,Status.bind(k2,k3)),_old, _rhs, st3)
      }

    case Arrow(ptr, field) =>
      for (
        (k1, _rhs, st1) <- rval(rhs, st0, ctx);
        (k2, _ptr, st2) <- rval(ptr, st1, ctx);
        (k3, _old, st3) <- st2 store (ctx arrow (_ptr, field), _rhs)
      ) yield {
        ctx.checkStore(_ptr.typ, field)
        (Status.bind(k1,Status.bind(k2,k3)), _old, _rhs, st3)
      }
  }

  // https://en.cppreference.com/w/cpp/language/eval_order
  def rval(expr: Expr, st0: State, ctx: Context): List[(Status, Pure, State)] = {
  log.debug(s"rval: expr = $expr, st0 = $st0");
  expr match {

    case BinOp(",", fst, snd) =>
      for (
        (status1, _fst, st1) <- rval(fst, st0, ctx);
        (status2, _snd, st2) <- rval(fst, st1, ctx)
      ) yield // monadic bind
        if (status1 == Status.ok) (status2, _snd, st2) else (status1, _fst, st1)

    case id: Id if (st0.store contains id) =>
      ctx.checkRead(id)
      List((Status.ok, st0 store id, st0))

    case id: Id if (ctx.consts contains id) =>
      List((Status.ok, ctx consts id, st0))

    case id: Id if ctx.mode == Mode.ghost && (ctx.sig contains id.name) =>
      val _fun = ctx sig id.name
      List((Status.ok, _fun(), st0))

    case id: Id =>
      throw error.InvalidProgram("invalid identifier", expr, st0)

    case Cast(typ, expr) => {
      val _typ = ctx.resolve(typ)
      for ((k, _expr, st1) <- rval(expr, st0, ctx))
        yield
          (k, Pure.coerce(_expr,_typ), st1)
    }
      
    case Lit(arg) =>
      List((Status.ok, Const(arg.toString, Sort.int), st0))

    case PreOp("&", id: Id) =>
      st0 takeAddress (id, ctx)


    case PreOp("&", PreOp("*", ptr)) =>
      rval(ptr, st0, ctx)

    case PreOp("&", Index(base, index)) =>
      val expr = BinOp("+", base, index)
      rval(expr, st0, ctx)

    case PreOp("&", Arrow(ptr, field)) =>
      for ((k, _ptr, st1) <- rval(ptr, st0, ctx)) yield {
        val _ptr_field = ctx arrow (_ptr, field)
        (k, _ptr_field, st1)
      }

    case PreOp("*", ptr) =>
      ctx.checkLoad()
      for ((k1, _ptr, st1) <- rval(ptr, st0, ctx);
           (k2, _res, st2) <- st1 load _ptr
      ) yield {
        (Status.bind(k1,k2), _res, st2)
      }

    case Index(arg, index) =>
      val expr = PreOp("*", BinOp("+", arg, index))
      rval(expr, st0, ctx)

    case Arrow(ptr, field) =>
      for ((k1, _ptr, st1) <- rval(ptr, st0, ctx);
           (k2, _res, st2) <- st1 load (ctx arrow (_ptr, field))
      ) yield {
        ctx.checkLoad(_ptr.typ, field)
        (Status.bind(k1,k2), _res, st2)
      }

    case PreOp("++", arg) =>
      for ((k, _, _rhs, st1) <- asg(arg, BinOp("+", arg, Lit(1)), st0, ctx))
        yield (k, _rhs, st1)
    case PreOp("--", arg) =>
      for ((k, _, _rhs, st1) <- asg(arg, BinOp("-", arg, Lit(1)), st0, ctx))
        yield (k, _rhs, st1)

    case PostOp("++", arg) =>
      for ((k, _val, _, st1) <- asg(arg, BinOp("+", arg, Lit(1)), st0, ctx))
        yield (k, _val, st1)
    case PostOp("--", arg) =>
      for ((k, _val, _, st1) <- asg(arg, BinOp("-", arg, Lit(1)), st0, ctx))
        yield (k, _val, st1)

    case BinOp("=", lhs, rhs) =>
      for ((k, _, _rhs, st1) <- asg(lhs, rhs, st0, ctx))
        yield (k, _rhs, st1)

    case BinOp("==", arg1, arg2) =>
      for ((k, List(_arg1, _arg2), st1) <- rvals(List(arg1, arg2), st0, ctx)) yield {
        (k, int_of_bool(_arg1 === Pure.coerce(_arg2,_arg1.typ)), st1)
      }

    case BinOp("!=", arg1, arg2) =>
      for ((k, List(_arg1, _arg2), st1) <- rvals(List(arg1, arg2), st0, ctx))
        yield (k, int_of_bool(_arg1 !== Pure.coerce(_arg2,_arg1.typ)), st1)

    case PreOp("~", arg) =>
      for ((k, _arg, st1) <- rval(arg, st0, ctx))
        yield (k, ~_arg, st1)

    case PreOp("!", arg) =>
      for ((k, _arg, st1) <- rval(arg, st0, ctx))
        yield (k, int_of_bool(!truth(_arg)), st1)

    case PreOp("-", arg) =>
      for ((k, _arg, st1) <- rval(arg, st0, ctx))
        yield (k, -_arg, st1)

    case BinOp("+", arg1, arg2) =>
      for ((k, List(_arg1, _arg2), st1) <- rvals(List(arg1, arg2), st0, ctx))
        yield (k, add(_arg1, _arg2, ctx), st1)

    case BinOp("-", arg1, arg2) =>
      for ((k, List(_arg1, _arg2), st1) <- rvals(List(arg1, arg2), st0, ctx))
        yield (k, sub(_arg1, _arg2, ctx), st1)

    case BinOp("*", arg1, arg2) =>
      for ((k, List(_arg1, _arg2), st1) <- rvals(List(arg1, arg2), st0, ctx))
        yield (k, _arg1 * Pure.coerce(_arg2,_arg1.typ), st1)

    case BinOp("/", arg1, arg2) =>
      for ((k, List(_arg1, _arg2), st1) <- rvals(List(arg1, arg2), st0, ctx))
        yield (k, _arg1 / Pure.coerce(_arg2,_arg1.typ), st1)

    case BinOp("%", arg1, arg2) =>
      for ((k, List(_arg1, _arg2), st1) <- rvals(List(arg1, arg2), st0, ctx))
        yield (k, _arg1 % Pure.coerce(_arg2,_arg1.typ), st1)

    case BinOp("&", arg1, arg2) =>
      for ((k, List(_arg1, _arg2), st1) <- rvals(List(arg1, arg2), st0, ctx))
        yield (k, _arg1 bitwise_& Pure.coerce(_arg2,_arg1.typ), st1)

    case BinOp("|", arg1, arg2) =>
      for ((k, List(_arg1, _arg2), st1) <- rvals(List(arg1, arg2), st0, ctx))
        yield (k, _arg1 bitwise_| Pure.coerce(_arg2,_arg1.typ), st1)

    case BinOp("^", arg1, arg2) =>
      for ((k, List(_arg1, _arg2), st1) <- rvals(List(arg1, arg2), st0, ctx))
        yield (k, _arg1 bitwise_^ Pure.coerce(_arg2,_arg1.typ), st1)

    case BinOp("<<", arg1, arg2) =>
      for ((k, List(_arg1, _arg2), st1) <- rvals(List(arg1, arg2), st0, ctx))
        yield (k, _arg1 << Pure.coerce(_arg2,_arg1.typ), st1)

    case BinOp(">>", arg1, arg2) =>
      for ((k, List(_arg1, _arg2), st1) <- rvals(List(arg1, arg2), st0, ctx))
        yield (k, _arg1 >> Pure.coerce(_arg2,_arg1.typ), st1)

    case BinOp("<=", arg1, arg2) =>
      for ((k, List(_arg1, _arg2), st1) <- rvals(List(arg1, arg2), st0, ctx))
        yield (k, int_of_bool(_arg1 <= Pure.coerce(_arg2,_arg1.typ)), st1)

    case BinOp("<", arg1, arg2) =>
      for ((k, List(_arg1, _arg2), st1) <- rvals(List(arg1, arg2), st0, ctx))
        yield (k, int_of_bool(_arg1 < Pure.coerce(_arg2,_arg1.typ)), st1)

    case BinOp(">=", arg1, arg2) =>
      for ((k, List(_arg1, _arg2), st1) <- rvals(List(arg1, arg2), st0, ctx))
        yield (k, int_of_bool(_arg1 >= Pure.coerce(_arg2,_arg1.typ)), st1)

    case BinOp(">", arg1, arg2) =>
      for ((k, List(_arg1, _arg2), st1) <- rvals(List(arg1, arg2), st0, ctx))
        yield (k, int_of_bool(_arg1 > Pure.coerce(_arg2,_arg1.typ)), st1)

    // don't fork if the rhs has no side effects
    case BinOp("||", arg1, arg2) if !Syntax.hasEffects(arg2) =>
      for (
        (k1, _arg1, st1) <- rval_test(arg1, st0, ctx);
        (k2, _arg2, st2) <- rval(arg2, st1, ctx)
      ) yield (Status.bind(k1,k2), truth(_arg1) || truth(_arg2), st2)

    case BinOp("&&", arg1, arg2) if !Syntax.hasEffects(arg2) =>
      for (
        (k1, _arg1, st1) <- rval_test(arg1, st0, ctx);
        (k2, _arg2, st2) <- rval(arg2, st1, ctx)
      ) yield (Status.bind(k1,k2), truth(_arg1) && truth(_arg2), st2)

    // shortcut evaluation yields two states
    case BinOp("||", arg1, arg2) =>
      val _arg1_st = rval_test(arg1, st0, ctx)

      val _true = for (
        (k1, _arg1, st1) <- _arg1_st;
        st1_true <- st1 && truth(_arg1)
      ) yield (k1, underflow.pure.True, st1_true)

      val _false = for (
        (k1, _arg1, st1) <- _arg1_st;
        st1_false <- st1 && !truth(_arg1);
        (k2, _arg2, st2) <- rval(arg2, st1_false, ctx)
      ) yield (Status.bind(k1,k2),_arg2, st2)

      _true ++ _false

    // shortcut evaluation yields two states
    case BinOp("&&", arg1, arg2) =>
      val _arg1_st = rval_test(arg1, st0, ctx)

      val _false = for (
        (k1, _arg1, st1) <- _arg1_st;
        st1_false <- st1 && !truth(_arg1)
      ) yield (k1, underflow.pure.False, st1_false)

      val _true = for (
        (k1, _arg1, st1) <- _arg1_st;
        st1_true <- st1 && truth(_arg1);
        (k2, _arg2, st2) <- rval(arg2, st1_true, ctx)
      ) yield (Status.bind(k1,k2), _arg2, st2)

      _false ++ _true

    case Question(test, left, right) if !Syntax.hasEffects(left) && !Syntax.hasEffects(right) => {
      val _test_st = rval_test(test, st0, ctx)

      val _run = for (
        (k1, _test, st1) <- _test_st;
        (k2, _left, st2) <- rval(left, st1, ctx);
        (k3, _right, st3) <- rval(right, st2, ctx)
      ) yield (Status.bind(k1,Status.bind(k2,k3)), truth(_test) ? (_left, _right), st1)

      val _err = for (
        (k, _test, st0) <- _test_st;
        st1 <- Exec.backprop(_test :!: st0.attacker, st0, ctx)
      ) yield (Status.bind(k,Status.err("insecure")), _test, st1)

      _err ++ _run
    }
    
    case Question(test, left, right) =>
      val _test_st = rval(test, st0, ctx)

      val _true = for (
        (k1, _test, st1) <- _test_st;
        st1_true <- st1 && truth(_test);
        (k2, _left, st2) <- rval(left, st1_true, ctx)
      ) yield (Status.bind(k1,k2), _left, st2)

      val _false = for (
        (k1, _test, st1) <- _test_st;
        st1_false <- st1 && !truth(_test);
        (k2, _right, st2) <- rval(right, st1_false, ctx)
      ) yield (Status.bind(k1,k2), _right, st2)

      val _err = for (
        (k, _test, st0) <- _test_st;
        st1 <- Exec.backprop(_test :!: st0.attacker, st0, ctx)
      ) yield (Status.bind(k,Status.err("insecure")), _test, st1)

      _err ++ _true ++ _false

    case FunCall(id, args) if ctx.mode == Mode.ghost && (ctx.sig contains id.name) =>
      val _fun = ctx sig id.name
      for ((k, _args, st1) <- rvals(args, st0, ctx)) yield {
        (k, app(_fun, _args: _*), st1)
      }

    case expr @ FunCall(id, args) =>
      if (!(ctx.specs contains id))
        throw error.InvalidProgram("no specification", expr)

      val specs = ctx specs id
      val Prepost(prePosts, fails, shared) = Prepost(specs)
      val isLemma = specs contains Lemma
      val isAtomic = specs contains AtomicSpec

      ctx.checkCall(expr, isLemma, isAtomic)

      val (typ, params, _) = ctx funs id
      val xr = ctx arbitrary (Id.result, typ)
      val ids = params map { case Formal(typ, name) => Id(name) }

      if (ids.length != args.length) {
        throw error.InvalidProgram("number of arguments does not match number of parameters", id, ids, args)
      }

      val as = rvals(args, st0, ctx);
      val asOKs = as.filter({ case (k, _, _) => k == Status.ok })
      val asErrs = as.filter({ case (k, _, _) => k != Status.ok})

      val es = for (
        (k, _args, st1) <- asErrs
      ) yield (k, xr, st1 eatAddrsTaken ctx)
      
      val fs = for (
        (k, _args, st1) <- asOKs;
        (pre,msg,post) <- fails;
        (fenv, fst3) <- consume(pre, st1.store ++ Store(Id.result :: ids, xr :: _args), st1, ctx);
        (_, fst4, _) <- produce(post, fenv, fst3, ctx, bind = false) // Note: ctx is unchanged for bind = false
      ) yield {
        (Status.err(msg),xr,fst4 eatAddrsTaken ctx)
      }

      val ss = for (
        (k, _args, st1) <- asOKs;
        (pre,post) <- prePosts;
        (env, st3) <- consume(pre, st1.store ++ Store(Id.result :: ids, xr :: _args), st1, ctx);
        (_, st4, _) <- produce(post, env, st3, ctx, bind = false) // Note: ctx is unchanged for bind = false
      ) yield {
        (Status.ok, xr, st4 eatAddrsTaken ctx)
      }
      es ++ fs ++ ss      
  }}

  /* def checkSharedCompatible(box0: Box, shared: Shared, params1: Store, ctx0: Context): Unit = {
    val st0 = State.default

    for (
      (env1, st1, ctx1) <- produce(box0.assrt, box0.params, st0, ctx0, bind = true);
      (st1_, st7) <- consume(shared.assrt, env1, st1, ctx1);
      (env2, st2, ctx2) <- produce(shared.assrt, params1, st7, ctx0, bind = true);
      (st2_, st9) <- consume(box0.assrt, env2, st2, ctx2);
      () = if (!st9.isPure) {
        throw error.VerificationFailure(
          "memory",
          "shared spec of called function is incompatible to shared spec of calling function")
      };

      (env3, st3, ctx3) <- produce(box0.assrt, box0.params, st0, ctx0, bind = true);
      (st3_, st8) <- consume(shared.assrt, env3, st3, ctx3);
      (env4, st4, ctx4) <- produce(shared.assrt, params1, st8, ctx0, bind = true);
      (st4_, _) <- consume(box0.assrt, env4, st4, ctx4)
    // no second check necessary, all possibilities are already covered by first test
    ) yield {
      val r0 = eval(box0.rely, st1.store, List(st3.store), ctx1)
      val r1 = eval(shared.rely, st1_, List(st3_), ctx2)
      val g0 = eval(box0.guarantee, st2.store, List(st4.store), ctx1)
      val g1 = eval(shared.guarantee, st2_, List(st4_), ctx2)

      prove((r0 ==> r1) && (g1 ==> g0), st0, ctx0)
    }
  }

  def instantiateRely(st0: State, ctx0: Context): List[State] = {
    // Mode.ghost required for havocBox only...
    // I think that the call to checkWrite in Context.arbitrary is at least debatable as
    // checkWrite checks for valid writes from the program perspective while arbitrary
    // is used internally?
    val st1 = st0.havocBox(ctx0 enter Mode.ghost)
    st1 && rely(st1.box, st0.store filterKeys st1.box.inst.keySet, ctx0);
  } */

  def eval_test(expr: Expr, st: Store, old: List[Store], ctx: Context) = {
    truth(eval(expr, st, old, ctx))
  }

  def axiom(expr: Expr, st: Store, ctx: Context): Pure = {
    val pure = eval(expr, st, Nil, ctx)
    if (!pure.free.isEmpty)
      throw error.InvalidProgram("invalid axiom", "free variables: " + pure.free)
    // ensure that function definitions are applied
    Simplify.simplify(pure, ctx.rewrites)
  }

  def rewrite(expr: Expr, st: Store, ctx: Context): Rewrite = eval(expr, st, Nil, ctx) match {
    case pure if !pure.free.isEmpty =>
      throw error.InvalidProgram("invalid rewrite", "free variables: " + pure.free)
    case Pure._eq(lhs, rhs) =>
      Rewrite(lhs, rhs)
    case Pure.imp(pre, Pure._eq(lhs, rhs)) =>
      Rewrite(lhs, rhs, Some(pre))
    case All(bound, Pure._eq(lhs, rhs)) =>
      Rewrite(lhs, rhs)
    case All(bound, Pure.imp(pre, Pure._eq(lhs, rhs))) =>
      Rewrite(lhs, rhs, Some(pre))

    case Pure.eqv(lhs, rhs) =>
      Rewrite(lhs, rhs)
    case Pure.imp(pre, Pure.eqv(lhs, rhs)) =>
      Rewrite(lhs, rhs, Some(pre))
    case All(bound, Pure.eqv(lhs, rhs)) =>
      Rewrite(lhs, rhs)
    case All(bound, Pure.imp(pre, Pure.eqv(lhs, rhs))) =>
      Rewrite(lhs, rhs, Some(pre))

    case _ =>
      throw error.InvalidProgram("invalid rewrite", "not one of", "lhs == rhs", "pre ==> lhs == rhs")
  }

  def rely(box: Box, inst0: Store, ctx: Context): Pure = {
    eval(box.rely, box.inst, List(inst0), ctx)
  }

  def guarantee(box: Box, inst1: Store, ctx: Context): Pure = {
    eval(box.guarantee, inst1, List(box.inst), ctx)
  }

  def eval(expr: Expr, st: Store, old: List[Store], ctx: Context): Pure = expr match {
    case id: Id if st contains id =>
      st(id)

    case id: Id if ctx.consts contains id =>
      ctx consts id

    case id: Id if ctx.sig contains id.name =>
      val _fun = ctx sig id.name
      _fun()

    case id: Id =>
      throw error.InvalidProgram("invalid identifier", expr, st)

    case Cast(typ, expr) => {
      val _typ = ctx.resolve(typ)
      val _expr = eval(expr, st, old, ctx)
      Pure.coerce(_expr,_typ)
    }

    case Lit(arg) =>
      Const(arg.toString, Sort.int)

    case PreOp("&", PreOp("*", ptr)) =>
      eval(ptr, st, old, ctx)
    // val _ptr = eval(ptr, st, old, ctx)
    // Ref(Loc.at(_ptr))

    case Dot(base, field) =>
      val _base = eval(base, st, old, ctx)
      ctx dot (_base, field)

    case PreOp("&", Arrow(ptr, field)) =>
      val _ptr = eval(ptr, st, old, ctx)
      ctx arrow (_ptr, field)

    case PreOp("-", arg) =>
      val _arg = eval(arg, st, old, ctx)
      -_arg

    case PreOp("+", arg) =>
      val _arg = eval(arg, st, old, ctx)
      +_arg

    case PreOp(op, arg) if (ctx.sig contains op) =>
      val _fun = ctx sig op
      val _arg = eval(arg, st, old, ctx)
      app(_fun, _arg)

    // polymorphic operators are treated explicitly
    case BinOp("==", arg1, arg2) =>
      val _arg1 = eval(arg1, st, old, ctx)
      val _arg2 = eval(arg2, st, old, ctx)
      int_of_bool(_arg1 === Pure.coerce(_arg2,_arg1.typ))

    case BinOp("!=", arg1, arg2) =>
      val _arg1 = eval(arg1, st, old, ctx)
      val _arg2 = eval(arg2, st, old, ctx)
      int_of_bool(_arg1 !== Pure.coerce(_arg2,_arg1.typ))

    case BinOp("::", arg1, arg2) =>
      val _arg1 = eval(arg1, st, old, ctx)
      val _arg2 = eval(arg2, st, old, ctx)
      _arg1 :: _arg2

    case BinOp(":!:", arg1, arg2) =>
      val _arg1 = eval(arg1, st, old, ctx)
      val _arg2 = eval(arg2, st, old, ctx)
      _arg1 :!: _arg2

    // addition/substraction are overloaded for integers/pointer arithmetic
    case BinOp("+", arg1, arg2) =>
      val _arg1 = eval(arg1, st, old, ctx)
      val _arg2 = eval(arg2, st, old, ctx)
      add(_arg1, _arg2, ctx)

    case BinOp("-", arg1, arg2) =>
      val _arg1 = eval(arg1, st, old, ctx)
      val _arg2 = eval(arg2, st, old, ctx)
      sub(_arg1, _arg2, ctx)

    case BinOp("<=", arg1, arg2) =>
      val _arg1 = eval(arg1, st, old, ctx)
      val _arg2 = eval(arg2, st, old, ctx)
      lower(_arg1, _arg2, ctx)

    case BinOp(op, arg1, arg2) if ctx.sig contains op =>
      val _fun = ctx sig op
      val _arg1 = eval(arg1, st, old, ctx)
      val _arg2 = eval(arg2, st, old, ctx)
      app(_fun, _arg1, if (_fun.args == List(Param.alpha,Param.alpha)) Pure.coerce(_arg2,_arg1.typ) else _arg2)

    case BinOp(op, arg1, arg2) =>
      throw error.InvalidProgram("undefined operator", op, ctx.sig)

    case Index(base, index) =>
      val _base = eval(base, st, old, ctx)
      val _index = eval(index, st, old, ctx)
      _base select _index

    case Update(base, index, arg) =>
      val _base = eval(base, st, old, ctx)
      val _index = eval(index, st, old, ctx)
      val _arg = eval(arg, st, old, ctx)
      _base store (_index, _arg)

    case FunCall(id, args) if ctx.sig contains id.name =>
      val _fun = ctx sig id.name
      val _args = args map (eval(_, st, old, ctx))
      app(_fun, _args: _*)

    case FunCall(id, args) =>
      throw error.InvalidProgram("undefined function", id, ctx.sig)

    case Question(test, left, right) =>
      val _test = eval_test(test, st, old, ctx)
      val _left = eval(left, st, old, ctx)
      val _right = eval(right, st, old, ctx)
      _test ? (_left, _right)

    case Old(inner) =>
      if (old.isEmpty)
        throw error.InvalidProgram("no old state", expr)
      val _st :: _old = old
      eval(inner, _st, _old, ctx)

    case Bind(how, params, body) =>
      val binding = params map {
        case Formal(typ, name) =>
          val id = Id(name)
          val sort = ctx resolve typ
          val x = Var(name, sort)
          (id -> x, x)
      }

      val (env, bound) = binding.unzip
      val _st = st ++ env

      how match {
        case "exists" => Ex(bound.toSet, eval(body, _st, old, ctx))
        case "forall" => All(bound.toSet, eval(body, _st, old, ctx))
      }

    case _ =>
      log.debug("unexpected expr: " + expr)
      ???
  }

  def open(pred: String, in: List[Expr], out: List[Expr], st: State, ctx: Context) = {
    val _pred = ctx preds pred
    val (xin, xout, body) = ctx defs pred
    val _in = in map (eval(_, st.store, st.old, ctx))
    val _out = out map (eval(_, st.store, st.old, ctx))
    val env = Store(xin ++ xout, _in ++ _out)
    (_pred, _in, _out, body, env)
  }
}
