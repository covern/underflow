package underflow.c

import java.math.BigInteger

import underflow.error

import underflow.pure.Pure

sealed trait Status

object Status {
  case object ok extends Status
  case object brk extends Status
  case object cnt extends Status  
  case class err(msg: String) extends Status
  case class ret(res: Option[Pure]) extends Status

  def bind(s1: Status, s2: Status): Status = {
    if (s1 == ok) s2 else s1
  }
}

object Exec {
  import Eval._
  import Verify._
  import Prove.prove
  import Prove.produce
  import Prove.consume

  var loop: Int = 1  // by default, only unfold loop bodies once
  var report_errors: Boolean = true
  var report_insecurity: Boolean = true

  def exec(stmts: List[Stmt], st: State, ctx: Context): List[(Status, State, Context)] = stmts match {
    case Nil =>
      List((Status.ok, st, ctx))
    case first :: rest =>
      exec(first, rest, st, ctx)
  }

  def exec(first: Stmt, rest: List[Stmt], st: State, ctx: Context): List[(Status, State, Context)] = {
    for (
      (k, st, ctx) <- exec(first, st, ctx);
      res <- exec(rest, k, st, ctx)
    ) yield res
  }

  def exec(first: Option[Stmt], st: State, ctx: Context): List[(Status, State, Context)] = first match {
    case None => List((Status.ok, st, ctx))
    case Some(first) => exec(first, st, ctx)
  }

  def exec(rest: List[Stmt], k: Status, st: State, ctx: Context): List[(Status, State, Context)] = k match {
    case Status.ok => exec(rest, st, ctx)
    case _ => List((k, st, ctx))
  }

  def exec(first: Stmt, st: State, ctx: Context): List[(Status, State, Context)] = {
    log.debug()
    log.debug("execute  ", first)
    log.debug("vars     ", ctx.vars)
    log.debug("ghost    ", ctx.ghost)
    log.debug("store    ", st.store)
    log.debug("path     ", st.path)
    log.debug("pto      ", st.heap.pto)
    log.debug("pinvalid ", st.heap.pinvalid)        
    log.debug("chunks   ", st.heap.chunks)
    log.debug("pre      ", st.pre)    

    trace.within(first, st) (st => {
      _exec(first, st, ctx)
    })
  }

  // do not attempt to prove phi. simplification of inferred contracts has to
  // be done at the end
  def backprop(phi: Pure, st: State, ctx: Context): List[State] = {
    if (phi != underflow.pure.True) {
      val _res = for (      
        st2 <- st.&&(phi);
        // no need to back-propagate Pure assertions 
        // unless we are in "strengthen" mode
        val backprop: Boolean = log.strengthen;
        val _ = log.debug(s"backprop: phi: $phi, shall I backprop? " + backprop);
        val st3 = if (backprop) st2.updateTrace(trace.backprop(phi,st2.tr)) else st2
      ) yield st3
      _res
    } else List(st)
  }
  
  def _exec(first: Stmt, st0: State, ctx0: Context): List[(Status, State, Context)] = first match {
    case Ghost(Apply(stmt)) =>
      for ((k, st1, ctx1) <- exec(stmt, st0, ctx0 enter Mode.ghost))
        yield (k, st1, ctx1.leave)

    case first: Global =>
      for ((k, st1, ctx1) <- exec(first, st0, ctx0))
        yield (k, st1, ctx1)

    case block @ Block(stmts) =>
      val vars = ctx0.vars
      val ghost = ctx0.ghost
      val env = st0.store filterKeys block.locals

      for ((k, st1, ctx1) <- exec(stmts, st0, ctx0))
        yield (k, st1 assign (env, ctx1), ctx1)

    case Break =>
      List((Status.brk, st0, ctx0))

    case Continue =>
      List((Status.cnt, st0, ctx0))

    case Return(None) =>
      List((Status.ret(None), st0, ctx0))

    case Return(Some(res)) =>
      for ((k, _res, st1) <- rval(res, st0, ctx0)) yield {
        // val st2 = st1 assign (Id.result, _res, ctx0)
        (Status.bind(k,Status.ret(Some(_res))), st1, ctx0)
      }

    case Atomic(expr) =>
      for ((k, _, st1) <- rval(expr, st0, ctx0))
        yield (k, st1, ctx0)

    case If(test, left, right) =>
      val _test_st = rval_test(test, st0, ctx0)
      val _test_st_oks = _test_st filter { case (k,_,_) => k == Status.ok };
      val _test_st_errs = _test_st filter { case (k,_,_) => k != Status.ok };
      val _terr = _test_st_errs map { case (k,_, st) => (k, st, ctx0) };
      
      // XXX: only need to explore the :!: low case if left and right are distinct
           
      val _err = if (report_insecurity) {
        for (
          (k, _test, st0) <- _test_st_oks;
          st1 <- backprop(_test :!: st0.attacker, st0, ctx0)
        ) yield (Status.bind(k,Status.err("insecure")), st1, ctx0)
      } else List()

      val _left = for (
        (_, _test, st0) <- _test_st_oks;
        st1 <- backprop(truth(_test), st0, ctx0);
        res <- exec(left, st1, ctx0)
      ) yield res

      val _right = for (
        (_, _test, st0) <- _test_st_oks;
        st1 <- backprop(!truth(_test), st0, ctx0);
        res <- exec(right, st1, ctx0)
      ) yield res

      _terr ++ _err ++ _left ++ _right

    case While(test, count, body) =>
      exec_while(test, count, body, st0, ctx0)

    case _ =>
      throw error.InvalidProgram("unsupported", first)
  }

  def exec_while(test: Expr, count: Int, body: Stmt, k: Status, st: State, ctx: Context): List[(Status, State, Context)] = k match {
    case Status.ok => exec_while(test, count, body, st, ctx)
    case Status.cnt => exec_while(test, count, body, st, ctx)    
    case Status.brk => List((Status.ok, st, ctx))
    case _ => List((k, st, ctx))
  }

  def exec_while(test: Expr, count: Int, body: Stmt, st0: State, ctx0: Context): List[(Status, State, Context)] = {
    val _test_st = rval_test(test, st0, ctx0)
    val _test_st_oks = _test_st filter { case (k,_,_) => k == Status.ok };
    val _test_st_errs = _test_st filter { case (k,_,_) => k != Status.ok };
    val _terr = _test_st_errs map { case (k,_, st) => (k, st, ctx0) };

    val _err = if (report_insecurity) {
      for (
        (_, _test, st0) <- _test_st_oks;
        st1 <- backprop(_test :!: st0.attacker, st0, ctx0)
      ) yield (Status.err("insecure"), st1, ctx0)
    } else List()

    val _left = if (count < loop) {
      for (
        (_, _test, st0) <- _test_st_oks;
        st1 <- backprop(truth(_test), st0, ctx0);
        (k2, st2, ctx2) <- exec(body, st1, ctx0);
        res <- exec_while(test, count + 1, body, k2, st2, ctx2)
      ) yield res
    } else {
      Nil
    }

    val _right = for (
      (k1, _test, st0) <- _test_st_oks;
      st1 <- backprop(!truth(_test), st0, ctx0)
    ) yield (k1, st1, ctx0)

    _terr ++ _err ++ _left ++ _right
  }

  def specify(stmts: List[Stmt], ctx: Context): Context = stmts match {
    case Nil =>
      ctx
    case FunDef(ret, id, params, specs, body) :: rest =>
      // if ((specs contains Lemma) && ret != Void)
      //   throw error.InvalidProgram("lemma has non void return type", ret)
      specify(
        rest,
        ctx copy (
          funs = ctx.funs + (id -> (ret, params, body)),
          specs = ctx.specs + (id -> specs)))
    case _ :: rest =>
      specify(rest, ctx)
  }

  def exec(first: Aux, st0: State, ctx0: Context): List[(Status, State, Context)] = first match {
    case Prune =>
      for (st1 <- st0.stronglyConsistent(ctx0))
        yield (Status.ok, st1, ctx0)

    case Loop(n: BigInteger) => {
      log.debug(s"setting loop bound to $n")
      loop = n.intValue()
      List((Status.ok, st0, ctx0))
    }
      
      
    case Produce(assrt) =>
      for ((_, st1, ctx1) <- produce(assrt, st0, ctx0, bind = true))
        yield (Status.ok, st1, ctx1)

    case Consume(assrt, msg) => {
      // currently we only support negating Expr asserts
      if (!assrt.isInstanceOf[Expr]) {
        throw error.InvalidProgram("Unsupported " + msg + ": " + assrt)
      }
      val oks = 
        for ((_, st2, ctx2) <- produce(assrt, st0, ctx0, bind = true)) yield {
          (Status.ok, st2, ctx2)
        }
      val errs =
        for ((_, st3, ctx3) <-
             produce(PreOp("!",(assrt.asInstanceOf[Expr])), st0, ctx0, bind = true)
      ) yield {
        (Status.err("assertion"), st3, ctx3)
      }
      /*
      log.debug("exec: Consume(" + assrt + ", " + msg + ")");
      log.shift {
        log.debug("ok posts: " + oks)
        log.debug("err posts: " + errs)
      }*/
      oks ++ errs
    }

    case Unfold(chunk @ Chunk(pred, in, out)) =>
      if (!(ctx0.defs contains pred))
        throw error.InvalidProgram("cannot unfold " + chunk + " (no definition)")

      val (_pred, _in, _out, body, env) = open(pred, in, out, st0, ctx0)
      val (_chunk, st1) = st0 access (_pred, _in)
      val eqs = Pure.eqs(_chunk.out, _out)
      prove(eqs, st1, ctx0)
      for ((_, st1, ctx1) <- produce(body, env, st1, ctx0, bind = false))
        yield (Status.ok, st1, ctx1)

/*
    case Fold(chunk @ Chunk(pred, in, out)) =>
      if (!(ctx0.defs contains pred))
        throw error.InvalidProgram("cannot fold " + chunk + " (no definition)")

      val (_pred, _in, _out, body, env0) = open(pred, in, out, st0, ctx0)
      val _chunk = underflow.heap.Chunk(_pred, _in, _out)

      for (
        (env1, st1) <- consume(body, env0, st0, ctx0);
        st2 <- st1 && _chunk
      ) yield {
        // Note: the env1 is irrelevant as all additions to st1 have been evaluated against it
        (Status.ok, st2, ctx0)
      }
*/
    case Unfold(assrt) =>
      throw error.InvalidProgram("cannot unfold " + assrt + " (not a predicate)")

    case Fold(assrt) =>
      throw error.InvalidProgram("cannot unfold " + assrt + " (not a predicate)")

    case PredDef(name, in, out, body) if (ctx0.preds contains name) =>
      throw error.InvalidProgram("predicate already defined", first)

    case PredDef(name, in, out, body) =>
      val ctx1 = ctx0 predicate (name, in, out, body)
      List((Status.ok, st0, ctx1))

    case PureDef(name, in, out, body) if (ctx0.sig contains name) =>
      throw error.InvalidProgram("function/constant already defined", name)

    case PureDef(name, in, out, body) =>
      val ctx1 = ctx0 function (name, in, out, body)
      List((Status.ok, st0, ctx1))

    case Rules(exprs, true) =>
      val axioms = exprs map (axiom(_, st0.store, ctx0))
      val ctx1 = ctx0 copy (axioms = ctx0.axioms ++ axioms)
      List((Status.ok, st0, ctx1))

    case Rules(exprs, false) =>
      val rewrites = exprs map (rewrite(_, st0.store, ctx0))
      val ctx1 = ctx0 copy (rewrites = ctx0.rewrites ++ rewrites)
      List((Status.ok, st0, ctx1))

    /* case BeginAtomicBlock =>
      for (
        (_, st1, ctx1) <- produce(st0.box, st0, ctx0 enter Mode.atomic, bind = true);
        // We cannot use st0.box because the last atomic block has modified the content
        st2 <- st1 && rely(st1.box, st0.store filterKeys st0.box.inst.keySet, ctx1)
      ) yield {
        (Status.ok, st2.resetAtomicCallCounter(), ctx1)
      }

    case EndAtomicBlock =>
      for (
        (env1, st1) <- consume(st0.box.assrt, st0, ctx0)
      ) yield {
        prove(guarantee(st0.box, env1 filterKeys st0.box.inst.keySet, ctx0), st1, ctx0)
        (Status.ok, st1 assign env1, ctx0.leave())
      } */

    case _ =>
      throw error.InvalidProgram("unsupported ghost statement", Ghost(first))
  }

  def exec(first: Global, st0: State, ctx0: Context): List[(Status, State, Context)] = first match {
    case first: Def =>
      List((Status.ok, st0, define(first, ctx0)))

    case Ghost(first) =>
      exec(first, st0, ctx0)

    case VarDef(typ, id, None, specs) =>
      val ctx1 = ctx0 declare List((id, typ),(Id("&"+id), PtrType(typ)))
      val st1 = st0 havoc (id, ctx1)
      List((Status.ok, st1, ctx1))

    case VarDef(typ, id, Some(init), specs) =>
      val ctx1 = ctx0 declare List((id, typ),(Id("&"+id), PtrType(typ)))    
      val st1 = st0 havoc (id, ctx1)
      for ((k, _init, st2) <- rval(init, st1, ctx1)) yield {
        val st3 = st2 assign (id, _init, ctx1)
        (k, st3, ctx1)
      }

    case FunDef(ret @ SignedInt, id @ Id.main, params @ List(), specs, Some(body)) =>
      verify(ret, id, params, specs, body, st0, ctx0)
      List((Status.ok, st0, ctx0))

    case FunDef(ret @ SignedInt, id @ Id.main, params @ List(Formal(SignedInt, argc), Formal(PtrType(SignedChar), argv)), specs, Some(body)) =>
      val st1 = st0 // TODO: add argv to the state
      val ctx1 = ctx0
      verify(ret, id, params, specs, body, st1, ctx1)
      List((Status.ok, st0, ctx0))

    //case FunDef(_, Id.main, _, _, _) =>
    //  throw error.InvalidProgram("invalid signature for main", first)

    case FunDef(ret, id, params, specs, None) =>
      List((Status.ok, st0, ctx0))

    case FunDef(ret, id, params, specs, Some(body)) =>
      // verify(ret, id, params, specs, body, ctx0.defaultState, ctx0)
      val ctx1 = verify(ret, id, params, specs, body, st0, ctx0)
      List((Status.ok, st0, ctx1))
  }

  def define(first: Def, ctx: Context): Context = first match {
    case TypeDef(typ, name) =>
      ctx copy (typedefs = ctx.typedefs + (name -> typ))

    case StructDecl(name) if ctx.structs contains name =>
      ctx
    case StructDecl(name) =>
      ctx copy (structs = ctx.structs + (name -> None))

    case UnionDecl(name) if ctx.unions contains name =>
      ctx
    case UnionDecl(name) =>
      ctx copy (unions = ctx.unions + (name -> None))

    case EnumDecl(name) if ctx.enums contains name =>
      ctx
    case EnumDecl(name) =>
      ctx copy (enums = ctx.enums + (name -> None))

    case StructDef(name, _) if (ctx.structs contains name) && ctx.structs(name) != None =>
      throw error.InvalidProgram("struct already defined", first)
    case StructDef(name, fields) =>
      ctx copy (structs = ctx.structs + (name -> Some(fields)))

    case UnionDef(name, _) if (ctx.unions contains name) && ctx.unions(name) != None =>
      throw error.InvalidProgram("union already defined", first)
    case UnionDef(name, fields) =>
      ctx copy (unions = ctx.unions + (name -> Some(fields)))

    case EnumDef(Some(name), enum) if (ctx.enums contains name) && ctx.enums(name) != None =>
      throw error.InvalidProgram("enum already defined", first)
    case EnumDef(None, consts) =>
      import underflow.pure.toConst
      val add = for ((name, index) <- consts.zipWithIndex)
        yield (Id(name), index: Pure)
      ctx copy (consts = ctx.consts ++ add)
    case EnumDef(Some(name), consts) =>
      val add = for ((name, index) <- consts.zipWithIndex)
        yield (Id(name), index: Pure)
      ctx copy (
        enums = ctx.enums + (name -> Some(consts)),
        consts = ctx.consts ++ add)
  }
}