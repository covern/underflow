package underflow.c

import underflow.error
import underflow.pure.Ex
import underflow.pure.Pure
import underflow.pure.Simplify
import underflow.pure.Sort
import underflow.pure.Var

/* consuming happens in three modes:
   (1) when proving that an initial state implies a presumption
       in this case we need to prove the assertions in the presumption
   (2) when proving that a final state is implied by a covers
       in this case we need to prove that the assertion implies the state's
       path condition
   (3) when consuming a function's presumption
       in this case we conjoin and back-propagate
*/
sealed trait ConsumeMode

object ConsumeMode {
  case object presumes extends ConsumeMode
  case object covers extends ConsumeMode
  case object backprop extends ConsumeMode
}


object Prove {
  import Eval._

  def reachable(phi: Pure, st: State, ctx: Context): List[State] = {
    // We are top-level, so this should never fail
    assert(
      phi.typ == Sort.bool,
      "not a formula: " + phi + ": " + phi.typ)

    // XXX evaluate reachability in relational mode always 
    val relational = true
    
    st.withSolver(relational, assumePath = false) {
      solver =>
        solver assume ctx.axioms
        val pre = Pure.and(st.path)
        // we need to existentially quantify over any local variables or other logical
        // variables introduced since the original presumption was consumed
        // except the logical variable introduced to store the function's return value
        // happily when this is called the store has been restored to such a state
        // so that it only maps such variables
        // NOTE: we don't want to existentially quantify over logical variables in the
        //       presumption, since those are considered part of the function's external contract
        //       again, happily, this approach won't do that since such variables will already
        //       have been bound (introduced) when the presumption was initially produced
        // the above discussion assumes we are in "validate" analysis mode of course
        assert(log.analysisMode == AnalysisMode.validate)
        val nonextvars = st.store.values.map(_.free).flatten
        log.debug("reachable: non-existential vars: " + nonextvars)
        val tobind = pre.free -- nonextvars
        log.debug("reachable: existential vars: " + tobind)
        val toprove = Ex(tobind,pre)

        log.debug("reachable: ", toprove + " <== " + phi)
        solver assume phi

        if (solver.isConsistent) {
          if (!solver.isValid(toprove)) {
            log.debug("reachable: solver said was not valid, so not reachable")
            // not reachable
            List()
          } else {
            // reachable
            log.debug("reachable: solver said was valid, so reachable")            
            List(st)
          }
        } else {
          // LHS of implies was false, so reachable
          log.debug("reachable: LHS of implies was inconsistent, so reachable")
          List(st)
        }
    }
  }

  def prove(phi: Pure, st: State, ctx: Context): List[State] = {
    // We are top-level, so this should never fail
    assert(
      phi.typ == Sort.bool,
      "not a formula: " + phi + ": " + phi.typ)

    val relational = phi.isRelational

    try {
    st.withSolver(relational) {
      solver =>
        val _phi = Simplify.simplify(phi, st.path, ctx.rewrites)
        if (_phi != phi) {
          log.debug("rewrite", phi + " ~> " + _phi)
        }

        solver assume ctx.axioms

        log.debug("prove", st.path.mkString(" && ") + " ==> " + _phi)
        _prove(_phi)

        def _prove(phi: Pure): Unit = phi match {
          case Pure.and(phi, psi) =>
            _prove(phi)
            _prove(psi)

          case Pure.haslabel(arg, sec) =>
            assert(relational)
            if (!(solver isValid phi)) {
              throw error.VerificationFailure("insecure", phi, st)
            }

          case _ =>
            if (!(solver isValid phi)) {
              if (phi.isRelational) // just look at this subformula
                throw error.VerificationFailure("insecure", phi, st)
              else
                throw error.VerificationFailure("incorrect", phi, st)
            }
        }
    }
    List(st)
    } catch {
      case e:Throwable =>
        log.debug("prove: caught exception " + e)
        List()
    }
  }

  // Note: guarantees that returned context is unchanged if bind == false
  // this fact is used for funcalls
  def produce(add: Assert, st: State, ctx: Context, bind: Boolean): List[(Store, State, Context)] = {
    val env: Store = st.store
    val inst: Store = Map()
    produce(add, env, inst, st, ctx, bind)
  }

  def produce(add: Assert, env: Store, st: State, ctx: Context, bind: Boolean): List[(Store, State, Context)] = {
    val inst: Store = Map()
    produce(add, env, inst, st, ctx, bind)
  }

  def produce(add: Assert, env: Store, inst: Store, st: State, ctx: Context, bind: Boolean): List[(Store, State, Context)] = {
    _produce(add, env, inst, st, ctx, bind)
  }

  def produce(box: Box, st0: State, ctx0: Context, bind: Boolean): List[(Store, State, Context)] = {
    for ((env1, st1, ctx1) <- produce(box.assrt, st0.box.params, st0, ctx0, bind)) yield {
      (env1, st1 updateBox env1, ctx1)
    }
  }

  def _produce(add: Assert, env0: Store, inst: Store, st0: State, ctx0: Context, bind: Boolean): List[(Store, State, Context)] = add match {
    case expr: Expr =>
      val _expr = eval(expr, env0, st0.old, ctx0)
      val _prop = truth(_expr)
      for (st1 <- st0 && _prop) yield (env0, st1, ctx0)

    case PointsTo(ptr, arg) =>
      val _ptr = eval(ptr, env0, st0.old, ctx0)
      val _arg = eval(arg, env0, st0.old, ctx0)
      val _prop = underflow.heap.PointsTo(_ptr, _arg)
      for (
        st1 <- st0 && _prop
      ) yield (env0, st1, ctx0)

    case PInvalid(ptr) =>
      val _ptr = eval(ptr, env0, st0.old, ctx0)
      val _prop = underflow.heap.PInvalid(_ptr)
      for (
        st1 <- st0 && _prop
      ) yield (env0, st1, ctx0)

    case Chunk(pred, in, out) =>
      val _in = in map (eval(_, env0, st0.old, ctx0))
      val _out = out map (eval(_, env0, st0.old, ctx0))
      val _pred = ctx0 preds pred
      val _prop = underflow.heap.Chunk(_pred, _in, _out)
      for (st1 <- st0 && _prop) yield (env0, st1, ctx0)

    /* case And(left: Expr, right: Expr) =>
      produce(new BinOp("&&", left, right), env0, inst, st0, ctx0, bind)

    case Cond(test, left: Expr, right: Expr) =>
      produce(new Question(test, left, right), env0, inst, st0, ctx0, bind) */

    case And(left, right) =>
      for (
        (env1, st1, ctx1) <- produce(left, env0, inst, st0, ctx0, bind);
        (env2, st2, ctx2) <- produce(right, env1, inst, st1, ctx1, bind)
      ) yield {
        (env2, st2, ctx2)
      }

    case Cond(test, left, right) =>
      val _test = eval_test(test, env0, st0.old, ctx0)

      val _left = for (
        st1 <- st0 && _test;
        res <- produce(left, env0, inst, st1, ctx0, bind)
      ) yield res

      val _right = for (
        st1 <- st0 && !_test;
        res <- produce(right, env0, inst, st1, ctx0, bind)
      ) yield res

      _left ++ _right

    case Exists(params, body) =>
      val _params = params.map {
        case Formal(typ, name) =>
          val id = Id(name)
          if (inst contains id)
            (id, inst(id))
          else
            (id, ctx0 arbitrary (id, typ))
      }

      val _types = params.map {
        case Formal(typ, name) =>
          val id = Id(name)
          (id, typ)
      }

      val _env = env0 ++ _params

      if (bind) {
        val ctx1 = ctx0 declareGhost _types
        val st1 = st0 assign (_params, ctx1)
        produce(body, _env, inst, st1, ctx1, bind)
      } else {
        produce(body, _env, inst, st0, ctx0, bind)
      }
  }

  def consumeCovers(rem: Assert, st0: State, ctx: Context): List[(Store, State)] = {
    val env0 = st0.store
    consume(rem, env0, st0, ctx, mode = ConsumeMode.covers)
  }

  def consumePresumption(rem: Assert, st0: State, ctx: Context): List[(Store, State)] = {
    val env0 = st0.store
    consume(rem, env0, st0, ctx, mode = ConsumeMode.presumes)
  }

  def consume(rem: Assert, env0: Store, st0: State, ctx: Context, mode: ConsumeMode = ConsumeMode.backprop): List[(Store, State)] = {
    val ex0: Set[Id] = Set()
    try {
      for ((ex1, cond1, rem1, env1, st1) <- consume(rem, ex0, env0, st0, ctx, mode);
           cond2 = rem1 map (eval_test(_, env1, st1.old, ctx));
           bound = ex1 map env1;
           xs = bound.asInstanceOf[Set[Var]];

           // constraints from matching points-to etc need to be proved
           prop1 = Ex(xs, Pure.and(cond1));
           val prop2 = Ex(xs, Pure.and(cond2));

           st2 <- mode match {
                    case ConsumeMode.backprop => Exec.backprop(prop1, st1, ctx)
                    case ConsumeMode.presumes => prove(prop1, st1, ctx);
                    case ConsumeMode.covers => prove(prop2 ==> prop1, st1, ctx)
                 };
           st3 <- mode match {
                    case ConsumeMode.covers => reachable(prop2, st2, ctx)
                    case ConsumeMode.backprop => Exec.backprop(prop2, st2, ctx)
                    case ConsumeMode.presumes => prove(prop2, st2, ctx)
                  }
           )
      yield {
        (env1, st3)
      }
    } catch {
      case e:error.VerificationFailure =>
        log.debug("consume: caught verification failure " + e)
        for (i <- e.getStackTrace()) {
          log.debug(i)
        }
        List()
    }
  }

  def bind(pat: Expr, arg: Pure, ex: Set[Id], env: Store, st: State, ctx: Context): (Set[Id], List[Pure], Store) = pat match {
    case id: Id if ex contains id =>
      if (env contains id) log.debug("rebinding", id, env(id), arg)
      (ex - id, Nil, env + (id -> arg))

    case _ =>
      val _pat = eval(pat, env, st.old, ctx)
      (ex, List(_pat === arg), env)
  }

  def bind(pats: List[Expr], args: List[Pure], ex0: Set[Id], env0: Store, st: State, ctx: Context): (Set[Id], List[Pure], Store) = (pats, args) match {
    case (Nil, Nil) =>
      (ex0, Nil, env0)

    case (arg :: args, pat :: pats) =>
      val (ex1, cond1, env1) = bind(arg, pat, ex0, env0, st, ctx)
      val (ex2, cond2, env2) = bind(args, pats, ex1, env1, st, ctx)
      (ex2, cond1 ++ cond2, env2)
  }

  def consume(rem: Assert, ex: Set[Id], env: Store, st: State, ctx: Context, mode: ConsumeMode): List[(Set[Id], List[Pure], List[Expr], Store, State)] = {
    log.debug("consuming", rem, st)
    _consume(rem, ex, env, st, ctx, mode)
  }

  def _consume(rem: Assert, ex0: Set[Id], env0: Store, st0: State, ctx: Context, mode: ConsumeMode): List[(Set[Id], List[Pure], List[Expr], Store, State)] = rem match {
    // Note: we defer the evaluation of these formulas
    //       hoping that we can get some more bindings for ex0
    case expr: Expr =>
      List((ex0, Nil, List(expr), env0, st0))

    case PointsTo(ptr, arg) =>
      val _ptr = eval(ptr, env0, st0.old, ctx)
      for ((_pto, st1) <- st0 access _ptr) yield {
        val pats = List(arg)
        val args = List(_pto.arg)
        val (ex1, cond1, env1) = bind(pats, args, ex0, env0, st1, ctx)
        (ex1, cond1, Nil, env1, st1)
      }

    case PInvalid(ptr) =>
      val _ptr = eval(ptr, env0, st0.old, ctx)
      for ((_pinvalid, st1) <- st0 accessInvalid _ptr) yield
      (ex0, Nil, Nil, env0, st1)

    case Chunk(pred, in, out) =>
      val _in = in map (eval(_, env0, st0.old, ctx))
      val _pred = ctx preds pred
      val (_chunk, st1) = st0 access (_pred, _in)
      val (ex1, cond1, env1) = bind(out, _chunk.out, ex0, env0, st1, ctx)
      List((ex1, cond1, Nil, env1, st1))

    /* case And(left: Expr, right: Expr) =>
      consume(new BinOp("&&", left, right), ex0, env0, st0, ctx, mode)

    case Cond(test, left: Expr, right: Expr) =>
      consume(new Question(test, left, right), ex0, env0, st0, ctx, mode) */

    case And(left, right) =>
      for (
        (ex1, cond1, rem1, env1, st1) <- consume(left, ex0, env0, st0, ctx, mode);
        (ex2, cond2, rem2, env2, st2) <- consume(right, ex1, env1, st1, ctx, mode)
      ) yield {
        (ex2, cond1 ++ cond2, rem1 ++ rem2, env2, st2)
      }

    case Cond(test, left, right) =>
      val _test = eval_test(test, env0, st0.old, ctx)

      val _left = for (
        st1 <- st0 && _test;
        res <- consume(left, ex0, env0, st1, ctx, mode)
      ) yield res

      val _right = for (
        st1 <- st0 && !_test;
        res <- consume(right, ex0, env0, st1, ctx, mode)
      ) yield res

      _left ++ _right

    case Exists(params, body) =>
      import underflow.SetOps
      val ex1 = Set(params map { case Formal(_, name) => Id(name) }: _*)
      assert(ex0 disjoint ex1)
      val env1 = params map (ctx arbitrary _)
      consume(body, ex0 ++ ex1, env0 ++ env1, st0, ctx, mode)
  }
}