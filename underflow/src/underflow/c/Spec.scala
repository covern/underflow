package underflow.c

import underflow.error

sealed trait Aux extends beaver.Symbol {
}

case class Produce(assrt: Assert) extends Aux {
  override def toString = "assume " + assrt
}

case class Loop(n: Any) extends Aux {
  override def toString = "loop " + (n.toString)
}

case class Consume(assrt: Assert, msg: String) extends Aux {
  override def toString = msg match {
    case "assert" => "assert " + assrt
    case _ => "assert " + assrt + " \"" + msg + "\""
  }
}

case class Unfold(assrt: Assert) extends Aux {
  override def toString = "unfold " + assrt
}

case class Fold(assrt: Assert) extends Aux {
  override def toString = "fold " + assrt
}

case object BeginAtomicBlock extends Aux {
  def self = this

  override def toString = "atomic begin"
}

case object EndAtomicBlock extends Aux {
  def self = this

  override def toString = "atomic end"
}

case class Apply(stmt: Stmt) extends Aux {
  override def toString = "apply " + stmt
}

case object Prune extends Aux {
  def self = this

  override def toString = "prune"
}

case class Rules(exprs: List[Expr], smt: Boolean) extends Aux {
  def this(exprs: Array[Expr], smt: Boolean) = this(exprs.toList, smt)

  override def toString = if (smt) {
    exprs.mkString("axioms ", "; ", ";")
  } else {
    exprs.mkString("rewrites ", "; ", ";")
  }
}

sealed trait Spec extends beaver.Symbol {
}

case class PresumesOk(pre: Assert, post: Assert) extends Spec {
  override def toString = "presumes " + pre + " covers " + post
}

case class Ensures(post: Assert) extends Spec {
  override def toString = "ensures " + post
}

case class Invariant(inv: Assert) extends Spec {
  override def toString = "invariant " + inv
}

case class Resource(inv: Assert) extends Spec {
  override def toString = "resource " + inv
}

case class Maintains(assrt: Assert) extends Spec {
  override def toString = "maintains " + assrt
}

case class Fails(pre: Assert, msg: String, post: Assert) extends Spec {
  override def toString = "presumes " + pre + " fails " + msg + " " + post
}

case object Lemma extends Spec {
  def self = this

  override def toString = "lemma"
}

case object AtomicSpec extends Spec {
  def self = this

  override def toString = "atomic"
}

case class Shared(assrt: Assert, rely: Expr, guarantee: Expr) extends Spec {
  override def toString = "shared " + assrt + " rely " + rely + " guarantee " + guarantee
}

case class PredDef(name: String, in: List[Formal], out: List[Formal], body: Option[Assert]) extends Aux {
  def this(name: String) = this(name, Nil, Nil, None)
  def this(name: String, body: Assert) = this(name, Nil, Nil, Some(body))
  def this(name: String, in: Array[Formal]) = this(name, in.toList, Nil, None)
  def this(name: String, in: Array[Formal], body: Assert) = this(name, in.toList, Nil, Some(body))
  def this(name: String, in: Array[Formal], out: Array[Formal]) = this(name, in.toList, out.toList, None)
  def this(name: String, in: Array[Formal], out: Array[Formal], body: Assert) = this(name, in.toList, out.toList, Some(body))

  override def toString = body match {
    case None => "predicate " + name + "(" + in.mkString(", ") + "; " + out.mkString(", ") + ")"
    case Some(body) => "predicate " + name + "(" + in.mkString(", ") + "; " + out.mkString(", ") + ")" + " " + body
  }
}

case class PureDef(name: String, in: List[Formal], out: Type, body: Option[Expr]) extends Aux {
  def this(name: String, out: Type) = this(name, Nil, out, None)
  def this(name: String, out: Type, body: Expr) = this(name, Nil, out, Some(body))
  def this(name: String, in: Array[Formal], out: Type) = this(name, in.toList, out, None)
  def this(name: String, in: Array[Formal], out: Type, body: Expr) = this(name, in.toList, out, Some(body))

  override def toString = (in, body) match {
    case (Nil, None) => "constant " + out + " " + name
    case (Nil, Some(body)) => "constant " + out + " " + name + "(" + in.mkString(", ") + ")" + " = " + body
    case (_, None) => "function " + out + " " + name + "(" + in.mkString(", ") + ")"
    case (_, Some(body)) => "function " + out + " " + name + "(" + in.mkString(", ") + ")" + " = " + body
  }
}

case class Prepost(prePosts: List[(Assert,Assert)], fails: List[(Assert,String,Assert)], shared: Shared) {
}

object Prepost {
  def apply(specs: List[Spec]): Prepost = {
    val prePosts = specs collect {
      case PresumesOk(pre,post) => (pre,post)
    }
    
    val fails = specs collect {
      case Fails(pre,msg,post) => (pre,msg,post)
    }

    val _shared: List[Shared] = specs collect {
      case s @ (Shared(_, _, _)) => s
    }
    val shared = if (_shared.isEmpty) {
      Shared(True, True, True)
    } else {
      _shared.head
    }

    Prepost(prePosts, fails, shared)
  }
}
