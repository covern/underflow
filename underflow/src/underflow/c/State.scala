package underflow.c

import underflow.error
import underflow.heap.Heap
import underflow.heap.Pred
import underflow.heap.Prop
import underflow.pure.Pure
import underflow.pure.Solver
import underflow.pure.Sort
import underflow.pure.Relational
import underflow.pure.Var

case class Box(assrt: Assert, rely: Expr, guarantee: Expr, inst: Store, params: Store) {
  def update(newInst: Store) = copy(inst = newInst filter { case (k,v) => inst contains k })
  override def toString = assrt.toString + " | " + inst.mkString(", ")
}

object Box {
  val empty = Box(True, True, True, Map(), Map())
}

case class State(
  attacker: Pure,
  path: List[Pure],
  store: Store,
  old: List[Store],
  heap: Heap,
  box: Box,
  numAtomicCalls: Int,
  solver: Relational,
  tr: List[(Stmt, State)],
  extprogvars: Set[Var] // tracks the logical variables used to represent the values
                        // of the "external" program variables (i.e. parameters + result)
                        // Should be used only for formatting found contracts
  ) {

  override def toString = {
    attacker + " | " + store.mkString(", ") + " | " + (path ++ heap.pto ++ heap.chunks).mkString(" && ")
  }

  def pre = prePost._1
    
  // Get the precondition in the trace and the postcondition in the state
  // XXX: this precondition refers to logical variables. We need to map
  //      those back to program variables, which we do by applying the
  //      substitution implied by the initial store in reverse
  def prePost: ((List[Pure],Heap),(List[Pure],Heap)) = tr match {
    case Nil => ((List(),Heap(List(),List(),List())),(List(),Heap(List(),List(),List())))
    case _ =>     
    val ph = tr.last._2.heap
    val pp = tr.last._2.path
    var su: Map[Var, Pure] = Map()
    val initStore = store // should equal to init store except having result
    initStore foreach (x => {
      val id = x._1
      val va = x._2
      //log.debug("pre: building subst, considering va: " + va)
      va match {
        case e@Var(_,_,_) =>
          if (extprogvars.contains(e)) {
            //log.debug("pre: adding to subst")
            su = su + ((e,Var(id.name,e.typ)))
          } else {
            //log.debug(s"pre: not adding to subst as it's not in extprogvars")
          }
        case _ => 
          //log.debug("pre: not adding to subst")
      }
    })
    //log.debug("pre: applying subst " + su)
    //log.debug("pre: ph before subst " + ph)
    //log.debug("pre: pp before subst " + pp)    
    val rh = ph.subst(su)
    val rp = pp.map(_.subst(su))
    //log.debug("pre: ph after subst " + rh)
    //log.debug("pre: pp after subst " + rp)

    ((rp,rh),(path.map(_.subst(su)),heap.subst(su)))
  }
  
  def info() {
    log.info("state")
    log.shift {
      log.info("store    ", store.mkString(", "))
      log.info("path     ", path.mkString(", "))
      log.info("pto      ", heap.pto.mkString(", "))
      log.info("pinvalid ", heap.pinvalid.mkString(", "))
      log.info("chunks   ", heap.chunks.mkString(", "))
      log.info("shared   ", box.toString)
      log.info("pre      ", pre)
      log.info("extpvars  ", extprogvars)
    }
  }

  def saveOld = {
    copy(old = store :: old)
  }

  def high = {
    copy(attacker = underflow.pure.High)
  }

  def extendTrace(stmt: Stmt, st: State): State =
    copy(tr = ((stmt, st) :: tr))

  def updateTrace(tr2: List[(Stmt,State)]) = {
    copy(tr = tr2)
  }

  def addExtProgVars(vs: Set[Id]) = {
    copy(extprogvars = extprogvars | vs.map(x => store(x).free).flatten)
  }
  
  def isPure = {
    heap == Heap.empty
  }

  def pure = {
    copy(heap = Heap.empty)
  }

  def updateBox(inst: Store) = {
    copy(box = box update inst)
  }

  def havocBox(ctx: Context): State = {
    val newInst = box.inst map (t => (t._1, ctx arbitrary t._1))
    updateBox(newInst) assign (newInst, ctx)
  }

  def getSolver(relational: Boolean) = {
    if (relational) solver else solver.inner
  }

  def withSolver[A](relational: Boolean, assumePath: Boolean = true)(f: Solver => A): A = {
    // XXX: Note that the non-relational solver cannot deal with Pure.haslabel (and polymorphic types in particular)
    var which = getSolver(relational)

    which scoped {
      which match {
        case which: Relational =>
          which assumeAttacker attacker
        case _ =>

      }

      if (assumePath)
        which assume path

      val ptrs = (heap.pto map (_.ptr)) ++ (heap.pinvalid map (_.ptr))

      if (!ptrs.isEmpty) {
        // println(ptrs)
        ptrs foreach {
          _.typ match {
            case Sort.pointer(elem) =>
            case _ => ???
          }
        }
        which assumeDistinct ptrs
      }

      f(which)
    }
  }

  def maybeConsistent: List[State] = {
    withSolver(relational = false) {
      solver =>
        if (solver.isConsistent) List(this)
        else Nil
    }
  }

  def stronglyConsistent(ctx: Context): List[State] = {
    withSolver(relational = true) {
      solver =>
        solver assume (ctx.axioms)

        if (solver.isConsistent) List(this)
        else Nil
    }
  }

  def check(phi: Pure): Boolean = {
    withSolver(phi.isRelational) {
      solver => solver isValid phi
    }
  }

  def &&(that: Pure) = {
    // XXX: when that is !(x :: low) this maybeConsistent always throws it away!
    //      so for now don't use it
    List(copy(path = that :: path)) 
  }


  // don't do consistency checking when conjoining new heap assertions
  // wait until we check for consistency at the end; otherwise too many solver
  // calls. For example when analysing one example (patched Lucky 13) this
  // blows out analysis time from 2 minutes to 8 minutes
  def &&(that: Prop) = {
    //copy(heap = heap && that) maybeConsistent    
    List(copy(heap = heap && that)) //maybeConsistent
  }
  def &&(that: Heap) = {
    //copy(heap = heap && that) maybeConsistent
    List(copy(heap = heap && that))
  }

  def assign(id: Id, arg: Pure, ctx: Context) = {
    val typ = ctx.resolve(if (ctx.vars contains id) ctx.vars(id) else ctx.ghost(id))
    copy(store = store + (id -> Pure.coerce(arg,typ)))
  }

  def assign(asg: Iterable[(Id, Pure)], ctx: Context) = {
    val _asg =for ((id,arg) <- asg) yield {
      val typ = ctx.resolve(if (ctx.vars contains id) ctx.vars(id) else ctx.ghost(id))
      (id, Pure.coerce(arg,typ))
    }
    copy(store = store ++ _asg)
  }

  def havoc(id: Id, ctx: Context) = {
    val pair = (id, ctx arbitrary id)
    copy(store = store + pair)
  }

  def havoc(ids: Iterable[Id], ctx: Context) = {
    val pairs = ids map (id => (id, ctx arbitrary id))
    copy(store = store ++ pairs)
  }

  def eatAddrsTaken(ctx: Context): State = {
    val pids = store filter { case (k,v) => k.name.startsWith("&") }
    // FIXME: rewrite this to use a fold-like thing instead
    var s = this
    for ((pid,ptr) <- pids) {
        //println(s"eatAddrsTaken: eating $pid -> $ptr")
        val id = Id(pid.name.substring(1))
        val (Some(underflow.heap.PointsTo(_,arg)), rest) = s.heap access (ptr, check)
        s = s.copy(heap = rest).assign(id,arg,ctx)
    }
    s.copy(store = s.store.filter { case (k,v) =>  !k.name.startsWith("&") })
  }
  
  def takeAddress(id: Id, ctx: Context): List[(Status,Pure,State)] = {
      if (!(store contains id)) {
        throw error.InvalidProgram("taking address of unknown variable", id)
      }
      (ctx resolve (ctx.vars(id))) match {
        case _:StructSort =>
          throw error.InvalidProgram("cannot take address of struct", id)
        case _ =>
      }
      val pid = Id("&"+id.name)      
      if (store contains pid) {
        List((Status.ok,store(pid),this))
      } else {
        val st1 = havoc (pid, ctx)
        val _pto = underflow.heap.PointsTo(st1.store(pid), st1.store(id))
        for (st2 <- st1.&&(_pto)) yield {
          (Status.ok,st1.store(pid),st2)
        }
      }
  }
  
  def load(ptr: Pure): List[(Status,Pure,State)] = {
    val typ = ptr.typ match {
      case Sort.pointer(typ) => typ
    }
    heap load (ptr, solver.isValid) match {
      case (Status.err(msg),opt) => {
        // we still have to return something well-typed here
        val v: Var = Var.fresh("x",typ)          
        List((Status.err(msg),v,copy())) 
      }
      case (Status.ok,Some(arg)) =>
        List((Status.ok,arg,copy()))
      case (Status.ok,None) => {
        // BPSE: for a fresh var v, back propagate ptr |-> v
        val v: Var = Var.fresh("x",typ)
        val _propok = underflow.heap.PointsTo(ptr, v)
        val _properr = underflow.heap.PInvalid(ptr)
        val stoks =
          for (st1ok <- this.&&(_propok)) yield {
          val st2ok = st1ok.updateTrace(trace.backprop(_propok,st1ok.tr));
          (Status.ok,v,st2ok)
        }
        import Exec.report_errors
        val sterrs = if (Exec.report_errors) {
            for (st1err <- this.&&(_properr)) yield {
            val st2err = st1err.updateTrace(trace.backprop(_properr,st1err.tr));
            (Status.err("memory"),v,st2err)
          }
        } else List()
        stoks ++ sterrs
      }
    }
  }

  def store(ptr: Pure, arg: Pure): List[(Status, Pure, State)] = {
    val typ = ptr.typ match {
      case Sort.pointer(typ) => typ
    }
    heap store (ptr, arg, solver.isValid) match {
      case (Status.err(msg),res) =>
        // return something well-typed to avoid errors elsewhere
        val v: Var = Var.fresh("x",typ)      
        List((Status.err(msg), v, copy()))
      case (Status.ok,Some((old, heap))) =>
        List((Status.ok, old, copy(heap = heap)))
      case (Status.ok,None) =>
        // BPSE: for a fresh var v, back propagate ptr |-> v
        val v: Var = Var.fresh("x",typ)
        val _propok = underflow.heap.PointsTo(ptr, v)
        val _newprop = underflow.heap.PointsTo(ptr, arg)
        val _properr = underflow.heap.PInvalid(ptr)
        val stoks =
          for (st1ok <- this.&&(_newprop)) yield {
          val st2ok = st1ok.updateTrace(trace.backprop(_propok,st1ok.tr));
          (Status.ok,v,st2ok)
        }
        import Exec.report_errors
        val sterrs = if (Exec.report_errors) {
            for (st1err <- this.&&(_properr)) yield {
            val st2err = st1err.updateTrace(trace.backprop(_properr,st1err.tr));
            (Status.err("memory"),v,st2err)
          }
        } else List()
        stoks ++ sterrs
    }
  }

  def access(ptr: Pure): List[(underflow.heap.PointsTo,State)] = {
    // if we can match a PInvalid we cannot match a PointsTo
    val (pinvalid, _) = heap accessInvalid (ptr, check)
    pinvalid match {
      case Some(pinvalid) =>
        List()
      case None =>
        val (pto, rest) = heap access (ptr, check)
        pto match {
          case None =>
            // XXX: BPSE add the needed assertion, back-propagate, and consume
            val typ = ptr.typ match {
              case Sort.pointer(typ) => typ
              case _ => println(s"access on unexpected ptr $ptr of type ${ptr.typ}"); ???
            }
            val v: Var = Var.fresh("x",typ)
            val _propok = underflow.heap.PointsTo(ptr, v)
            val stoks =
            for (st1ok <- this.&&(_propok)) yield {
              val st2ok = st1ok.updateTrace(trace.backprop(_propok,st1ok.tr));
              st2ok.access(ptr)
            }
            stoks.flatten
          case Some(pto) =>
            List((pto, copy(heap = rest)))
        }
    }
  }

  def accessInvalid(ptr: Pure): List[(underflow.heap.PInvalid,State)] = {
    // if we can match a PointsTo we cannot match a PInvalid
    val (pto, _) = heap access (ptr, check)
    pto match {
      case Some(pto) =>
        List()
      case None =>
        val (pinvalid, rest) = heap accessInvalid (ptr, check)
        pinvalid match {
          case None =>
            // XXX: BPSE add the needed assertion, back-propagate, and consume
            val _properr = underflow.heap.PInvalid(ptr)
            val sterrs =
            for (st1err <- this.&&(_properr)) yield {
              val st2err = st1err.updateTrace(trace.backprop(_properr,st1err.tr));
              st2err.accessInvalid(ptr)
            }
            sterrs.flatten

          case Some(pinvalid) =>
            List((pinvalid, copy(heap = rest)))
        }
    }
  }

  def access(pred: Pred, in: List[Pure]) = {
    val (chunk, rest) = heap access (pred, in, check)
    chunk match {
      case None =>
        throw error.VerificationFailure("memory", "cannot find ", pred + "(" + in.mkString(",") + "; ?)", this)
      case Some(chunk) =>
        (chunk, copy(heap = rest))
    }
  }

  def resetAtomicCallCounter(): State = {
    copy(numAtomicCalls = 0)
  }

  def doAtomicCall(): State = {
    if (numAtomicCalls != 0) {
      throw error.VerificationFailure("effects", "multiple atomic calls in same atomic block")
    }
    copy(numAtomicCalls = numAtomicCalls + 1)
  }
}

object State {
  def empty = State(
    attacker = underflow.pure.Attacker,
    path = Nil,
    store = Map.empty,
    old = Nil,
    heap = Heap.empty,
    box = Box.empty,
    numAtomicCalls = 0,
    solver = Solver.relational,
    tr = List(),
    extprogvars = Set())

  def default = empty
}