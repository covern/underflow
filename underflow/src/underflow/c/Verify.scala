package underflow.c

import underflow.error

import underflow.pure.Pure
import underflow.heap.Heap
import underflow.pure.Var
import java.util.HashSet

object Verify {
  import Eval._
  import Exec._
  import Prove.prove
  import Prove.produce
  import Prove.consumeCovers
  import Prove.consumePresumption
  
  var interactive: Boolean = true

  def file(name: String, stmts: List[Global], pTypes: HashSet[String], pPreds: HashSet[String]): Unit = {
    log.shift {
      val _ctx0 = Context.default copy (parserTypes = pTypes, parserPreds = pPreds)
      val ctx0 = specify(stmts, _ctx0)
      val st0 = ctx0.defaultState
      verify(stmts, st0, ctx0)
    }
  }

  def verify(stmts: List[Global], st0: State, ctx0: Context): Unit =
  stmts match {
    case Nil =>
    case first :: rest =>
      for ((k, st1, ctx1) <- exec(first, st0, ctx0)) {
        verify(rest, st1, ctx1)
      }
  }

  def prePostToAssertStrings(prepost: ((List[Pure],Heap),(List[Pure],Heap)), extprogids: Set[String]): (String,String) = {
    // do precondition 
    val path = prepost._1._1
    val heap = prepost._1._2.sort
    val spath = path.map(_.cString).mkString(" && ")
    val spto = heap.pto.map(_.cString).mkString(" &*& ")
    val spinvalid = heap.pinvalid.map(_.cString).mkString(" &*& ")
    val _bodymsg = List(spath,spto,spinvalid).filter(_ != "").mkString(" &*& ")
    
    // existentially quantify all non-external, non-program variables
    val vars: Set[underflow.pure.Var] = (path.map(_.free).flatten ++ heap.pto.map(_.free).flatten ++ heap.pinvalid.map(_.free).flatten).toSet
    log.debug("prePostToAssertString: all vars: " + vars)
    val exvars = vars.filter((a => !(extprogids.contains(a.toString))))
    log.debug("prePostToAssertString: existentially quantified vars: " + exvars)                
    val _exmsg: String = exvars.toList.map((x => x.typ.cTypeString + " " + x.cString)).mkString(", ")
    val exmsg = if (_exmsg != "") "exists " + _exmsg + ". " else ""

    assert (!((_bodymsg == "") && (exmsg != "")))
    val premsg = if (_bodymsg == "") "true" else exmsg ++ _bodymsg
    
    // do postcondition
    val path2 = prepost._2._1
    val heap2 = prepost._2._2.sort
    val spath2 = path2.map(_.cString).mkString(" && ")
    val spto2 = heap2.pto.map(_.cString).mkString(" &*& ")
    val spinvalid2 = heap2.pinvalid.map(_.cString).mkString(" &*& ")
    val _bodymsg2 = List(spath2,spto2,spinvalid2).filter(_ != "").mkString(" &*& ")

    // existentially quantify all non-external, non-program variables not quantified in presumption
    val vars2: Set[underflow.pure.Var] = (path2.map(_.free).flatten ++ heap2.pto.map(_.free).flatten ++ heap2.pinvalid.map(_.free).flatten).toSet
    log.debug("prePostToAssertString: all vars (post): " + vars)
    val exvars2 = vars2.filter((a => (!extprogids.contains(a.toString) && !exvars.map(_.toString).contains(a.toString))))    
    log.debug("prePostToAssertString: existentially quantified vars (post): " + exvars2)                
    val _exmsg2: String = exvars2.toList.map((x => x.typ.cTypeString + " " + x.cString)).mkString(", ")
    val exmsg2 = if (_exmsg2 != "") "exists " + _exmsg2 + ". " else ""

    assert (!((_bodymsg2 == "") && (exmsg2 != "")))
    val postmsg = if (_bodymsg2 == "") "true" else exmsg2 ++ _bodymsg2

    val r = (premsg, postmsg)
    log.debug(s"prePostToAssertString: result: $r")
    r
  }

  /* return updated context with newly discovered contracts when in that mode */
  def verify(typ: Type, id: Id, params: List[Formal], specs: List[Spec], body: Stmt, st0: State, ctx0: Context): Context = {
    log.info(id + " ... ")

    val start = System.currentTimeMillis()

    val globalIds: Set[Id] = st0.store.keys.toSet
    
    // at present we do not support global variables
    if (!globalIds.isEmpty)
      throw error.InvalidProgram("Global variables not supported!")
    
    val Prepost(prePosts, fails, _) = Prepost(specs)

    val okContracts = for ((pre,post) <- prePosts) yield (pre,Status.ok,post)
    val failContracts = for ((pre,msg,post) <- fails) yield (pre,Status.err(msg),post)
    
    val contracts =
      if (log.analysisMode == AnalysisMode.validate)
        okContracts ++ failContracts
      else
        // a dummy contract 
        List((True,Status.ok,True))
    
    val _params = (params map { case Formal(typ, name) => (Id(name), typ) })
    val _pparams = (_params map { case (Id(name), typ) => (Id("&"+name), PtrType(typ)) })
    
    val ctx = ctx0
    var updatedContext = ctx0

    val ctx1 = ctx declare (_params ++ _pparams) declareGhost (Id.result, typ)

    val _ids = _params map (_._1)
    val st1 = (st0 havoc (_ids, ctx1)).addExtProgVars(_ids.toSet)

    // number of unreachable / discovered contracts, depending on the mode
    var count = 0
        
    try {
      for ((pre,expected,post) <- contracts) {
        log.debug(s"Exploring contract pre = $pre, expected = $expected, post = $post")
        val finals = for (
          (_:Store,st2:State,ctx2:Context) <- if (log.analysisMode == AnalysisMode.validate)
                            produce(pre, st1, ctx1, bind = true)
                          else
                            List((st1.store, st1, ctx1));
          (k, _st3, ctx3) <- exec(body, st2, ctx2)
        ) yield {
          // overwrite all variables with old versions
          // because postconditions always refer to the initial values
          val st3 = _st3 copy (store = st2.store)
                
          k match {
            case Status.brk =>
              throw error.InvalidProgram("break outside of loop", body)

            case Status.ret(None) =>
              (Status.ok, st3, ctx3)

            case Status.ret(Some(res)) => {
              // we cannot simply bind "result" to the returned value, because
              // this final store is also used to evaluate the covers when
              // proving that the covers implies the path condition here and
              // in the covers we should not assume that "result" has the
              // value it was assigned here (otherwise that makes it stronger
              // than it really is intended to be). Therefore we introduce
              // a new logical variable to refer to the result and add a
              // path condition that states it is equal to the value here
              val retTyp = ctx3.resolve(typ)
              val v: Var = Var.fresh("result",retTyp)
              val st4:State = st3 assign (Id.result, v, ctx3)
              // first add "result" and then v will be a new extvar
              val st5:State = st4.addExtProgVars(Set(Id.result))
              val st6s = (st5 && (v === Pure.coerce(res,retTyp)))
              assert (st6s.length == 1)
              (Status.ok, st6s.head, ctx3)
            }

            case Status.ok =>
              (Status.ok, st3, ctx3)

            case Status.err(msg) =>
              (Status.err(msg), st3, ctx3)
          }
        }

        val conFinals = finals.filter({ case (k, st, ctx) => !st.stronglyConsistent(ctx).isEmpty})      
        if (log.analysisMode == AnalysisMode.validate) {
          log.debug("Looking for satisfaction of: ")
          log.debug("   pre: " + pre)
          log.debug("status: " + expected)
          log.debug("  post: " + post)
          val ok = conFinals exists {
            case (k, st, ctx) => {
              log.debug(s"         checking: k: $k, st: $st, ctx: $ctx")
              val b =
                (k == expected && covers(k, post, st, ctx) &&
                 presumes(pre, st.tr.last._2, ctx))
              log.debug(s"got it? $b")
              b
            }
            case e@_ => log.debug(s"XXX got something $e"); false
          }

          if (!ok) {
            val m = expected match { case Status.ok => "covers" case Status.err(msg) => "fails " + msg }
            log.info(s"  unreachable: _(presumes $pre $m $post)")
            count = count + 1
          }
        } else {
          for ((k, stend, ctxend) <- conFinals) {
            if (log.level >= log.Debug) {
              log.debug("Final consistent state discovered: ")
              stend.info()
            }
            
            val sPrePost = stend.prePost

            val extprogidnames = (params.map(_.name).toSet) + Id.result.name
            val (prestr,poststr) = prePostToAssertStrings(sPrePost,extprogidnames)

            val m = k match { case Status.ok => "covers" case Status.err(msg) => "fails " + msg }
            val str = s"  _(presumes $prestr $m $poststr)"
            log.info(str)
            val s:Spec = parseSpecFromString(str,ctxend.parserTypes,ctxend.parserPreds)
            val curspecs:List[Spec] = updatedContext.specs(id)
            val newspecs = s::curspecs
            updatedContext = updatedContext copy (        
              specs = updatedContext.specs + (id -> newspecs)
            )

            count = count + 1
          }
        }
      }
      
      val end = System.currentTimeMillis()
      val time = (end - start)

      if (log.analysisMode == AnalysisMode.validate) {
        if (count > 0){
          assert(count <= contracts.length)
          val sat = contracts.length - count
          log.info_(s"unreachable contracts, $sat satisfied ⚡ ")
        } else {
          log.info_("success ❤ ") 
        }        
      } else {
        if (log.analysisMode == AnalysisMode.discover) {
          log.info_(s"discovered $count contracts ❤ ")
        }
      }
      log.info(s"(time $time ms)")
      log.info(st0.solver.printSMTStatistics)
    } catch {
      case e @ trace.VerificationFailure(trace, msg, info @ _*) =>
        log.info(msg + " ⚡")
        log.shift {
          for (more <- info) {
            log.info(more)
          }
          log.info("reverse trace (last statement first)")
          log.shift {
            for ((stmt, st) <- trace) {
              st.info()
              log.info("execute")
              log.shift {
                log.info(stmt)
              }
            }
          }
        }
        if (!interactive)
          throw e

      case e @ error.VerificationFailure(msg, info @ _*) =>
        log.info(msg + " ⚡")
        for (more <- info) {
          log.info("  " + more)
        }
        if (!interactive)
          throw e

      case e: error.VerificationFailure =>
        log.info("unknown verification failure ⚡")
        if (!interactive)
          throw e

      case e @ error.Error(msg, info @ _*) =>
        log.info(msg + " ⚡")
        for (more <- info) {
          log.info("  " + more)
        }
        if (!interactive)
          throw e
    }
    // return updated context
    updatedContext
  }

  def covers(k: Status, post: Assert, st0: State, ctx0: Context): Boolean = {
    log.debug("covers: " + k + ", " + post + ", " + st0)
    try {
      var ok = false
      for ((_, st1) <- consumeCovers(post, st0, ctx0)) {
        /* if we leak heap chunks, we don't actually pass the covers check of course */
        if (st1.isPure) {
          ok = true
          val chunks = st1.heap.pto ++ st1.heap.chunks
          //throw error.VerificationFailure("memory", "leaking heap chunks", chunks.mkString(" && "), "postcondition: " + post, st0)
        }
      }

      ok
    } catch {
      case e: error.VerificationFailure =>
        log.debug("covers: got failure during consume " + e)
        false
    }
  }

  def presumes(pre: Assert, st0: State, ctx0: Context): Boolean = {
    log.debug("pre: " +  pre + ", " + st0)
    try {
      var ok = false
      for ((_, st1) <- consumePresumption(pre, st0, ctx0)) {
        /* if we leak heap chunks, we don't actually pass the presumption check because
           it means we likely had to infer extra heap chunks */
        if (st1.isPure) {
          ok = true
          val chunks = st1.heap.pto ++ st1.heap.chunks
          //throw error.VerificationFailure("memory", "leaking heap chunks", chunks.mkString(" && "), "postcondition: " + post, st0)
        }
      }

      ok
    } catch {
      case e: error.VerificationFailure =>
        log.debug("presumes: got failure during consume " + e)
        false
    }
  }

}
