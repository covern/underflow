package underflow

import java.io.File
import java.io.FileReader
import java.io.StringReader
import java.io.InputStreamReader
import java.io.Reader
import java.util.HashSet

import scala.collection.JavaConverters.asScalaBufferConverter

package object c {
  import underflow.pure.Pure
  type Store = Map[Id, Pure]

  object Store {
    def apply(ids: Iterable[Id], args: Iterable[Pure]) = {
      val st = (ids zip args)
      st.toMap
    }
  }

  val NULL = Id("NULL")
  val High = Id("high")
  val Low = Id("low")
  val False = Id("false")
  val True = Id("true")
  val TID = Id("TID")

  def verify(path: String) {
    val (stmts,types,preds) = parse(path)
    Verify.file(path, stmts, types, preds)
  }

  def parse(): (List[Global],HashSet[String],HashSet[String]) = {
    parse(new InputStreamReader(System.in), "-")
  }

  def parse(path: String): (List[Global],HashSet[String],HashSet[String]) = {
    parse(new FileReader(path), path)
  }

  def parse(file: File): (List[Global],HashSet[String],HashSet[String]) = {
    parse(new FileReader(file), file.getPath)
  }

  def parseSpecFromString(str: String, types: HashSet[String], preds: HashSet[String]): Spec = {
    parse(new StringReader(str), str, Some(types), Some(preds))._1.head
  }

  // return the types and preds found so we can use them again later if needed
  def parse[A](reader: Reader, path: String, _types: Option[HashSet[String]] = None, _preds: Option[HashSet[String]] = None): (List[A],HashSet[String],HashSet[String]) = {
    val scanner = new Scanner(reader)
    val parser = new Parser()

    
    val types: HashSet[String] = _types match {
      case None => new java.util.HashSet[String]
      case Some(t) => t
    }
                             
    val preds: HashSet[String] = _preds match {
      case None => new java.util.HashSet[String]
      case Some(p) => p
    }

    types.add("bool")
    types.add("sec")

    scanner.types = types
    scanner.preds = preds

    parser.types = types
    parser.preds = preds

    val result = try {
      parser parse scanner
    } catch {
      case e: Exception =>
        throw error.InvalidProgram("parse", path, e)
    }

    if (result != null) {
      
      val res: List[A] =
      try {
        result.asInstanceOf[java.util.ArrayList[A]].asScala.toList
      } catch {
        case e:Throwable =>
          List(result.asInstanceOf[A])
      }
      (res,types,preds)
    } else {
      (Nil,types,preds)
    }
  }
}