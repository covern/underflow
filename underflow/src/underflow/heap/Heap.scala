package underflow.heap

import underflow.error
import underflow.pure.Pure
import underflow.pure.Ren
import underflow.pure.Subst
import underflow.c.Status

case class Heap(pto: List[PointsTo], pinvalid: List[PInvalid], chunks: List[Chunk]) {
  def free = Set((pto flatMap (_.free)) ++ (pinvalid flatMap (_.free)) ++ (chunks flatMap (_.free)): _*)
  def rename(re: Ren) = Heap(pto map (_ rename re), pinvalid map (_ rename re), chunks map (_ rename re))
  def subst(su: Subst) = Heap(pto map (_ subst su), pinvalid map (_ subst su), chunks map (_ subst su))

  def hasPath(p1: Pure, p2: Pure): Boolean = {
    if (p1 == p2) true
    else {
      val nexts = pto.filter(_.ptr == p1).map(_.arg)
      if (nexts.isEmpty) false
      else {
        nexts.map(x => hasPath(x,p2)) reduce (_ || _)
      }
    }
  }
  def lessThan(pto1: PointsTo, pto2: PointsTo) = (pto1, pto2) match {
    case (PointsTo(ptr1,arg1),PointsTo(ptr2,arg2)) =>
      hasPath(arg1,ptr2)
  }
  
  // sort the ptos so that if we have x |-> y,   z |-> x, we will end up with
  //   z |-> x, x |-> y 
  def sort: Heap = {
    copy(pto = pto sortWith lessThan)
  }
   
  // def ptrs = pto map (_.ptr)

  /** Prepend prop to either pto, pinvalid or chunks */
  def &&(prop: Prop): Heap = prop match {
    case prop: PointsTo => Heap(prop :: pto, pinvalid, chunks)
    case prop: PInvalid => Heap(pto, prop :: pinvalid, chunks)
    case prop: Chunk    => Heap(pto, pinvalid, prop :: chunks)
  }
  
  def &&(that: Heap): Heap = {
    Heap(this.pto ++ that.pto, this.pinvalid ++ that.pinvalid, this.chunks ++ that.chunks)
  }

  /** Prepend props to this heap, but keep the order of props (no reversing) */
  def &&(props: List[Prop]): Heap = props match {
    case Nil =>
      this
    case prop :: rest =>
      this && rest && prop
  }

  def invalid(ptr: Pure, test: Pure => Boolean): Boolean = {
    ((pinvalid find (pinvalid => pinvalid.ptr.typ == ptr.typ && test(pinvalid.ptr === ptr))) != None)
  }
  
  def load(ptr: Pure, test: Pure => Boolean): (Status,Option[(Pure)]) = {
    if (invalid(ptr, test)) {
      (Status.err("memory"),None)      
    } else {
      val opt = for (PointsTo(_, arg) <- pto find (pto => pto.ptr.typ == ptr.typ && test(pto.ptr === ptr))) yield {
        (arg)
      }
      (Status.ok,opt)
    }
  }

  def store(ptr: Pure, arg: Pure, test: Pure => Boolean): (Status,Option[(Pure, Heap)]) = {
    if (invalid(ptr, test)) {
      (Status.err("memory"),None)
    } else {
      val (res, rest) = access(ptr, test)
      res match {
        case None => (Status.ok,None)
        case Some(PointsTo(_, old)) =>
          val pto = PointsTo(ptr, arg)
          (Status.ok,Some((old, rest && pto)))
      }
    }
  }
  
  def accessInvalid(ptr: Pure, test: Pure => Boolean): (Option[PInvalid], Heap) = {
    val (res, rest) = Heap.accessInvalid(ptr, pinvalid, test)
    (res, Heap(pto, rest, chunks))
  }
  
  def access(ptr: Pure, test: Pure => Boolean): (Option[PointsTo], Heap) = {
    val (res, rest) = Heap.access(ptr, pto, test)
    (res, Heap(rest, pinvalid, chunks))
  }

  def access(pred: Pred, in: List[Pure], test: Pure => Boolean): (Option[Chunk], Heap) = {
    val (res, rest) = Heap.access(pred, in, chunks, test)
    (res, Heap(pto, pinvalid, rest))
  }
}

object Heap {
  val empty = Heap(Nil, Nil, Nil)

  /**
   * Find and remove a points to assertion from the given heap,
   * such that its location is equal to loc according to the provided test predicate.
   *
   * If such an assertion exists, the result is that assertion and the remaining heap without it,
   * otherwise, the original heap is returned.
   */
  def access(ptr: Pure, heap: List[PointsTo], test: Pure => Boolean): (Option[PointsTo], List[PointsTo]) = heap match {
    case Nil =>
      (None, heap)
    case pto :: heap if pto.ptr.typ == ptr.typ && test(pto.ptr === ptr) =>
      // XXX: these two types might mismatch when fields are in play
      (Some(pto), heap)
    case pto :: heap =>
      val (res, rest) = access(ptr, heap, test)
      (res, pto :: rest)
  }

  def accessInvalid(ptr: Pure, heap: List[PInvalid], test: Pure => Boolean): (Option[PInvalid], List[PInvalid]) = heap match {
    case Nil =>
      (None, heap)
    case pinvalid :: heap if pinvalid.ptr.typ == ptr.typ && test(pinvalid.ptr === ptr) =>
      // XXX: these two types might mismatch when fields are in play
      (Some(pinvalid), heap)
    case pinvalid :: heap =>
      val (res, rest) = accessInvalid(ptr, heap, test)
      (res, pinvalid :: rest)
  }

  def access(pred: Pred, in: List[Pure], heap: List[Chunk], test: Pure => Boolean): (Option[Chunk], List[Chunk]) = heap match {
    case Nil =>
      (None, heap)
    case chunk :: heap if chunk.pred == pred && test(Pure.eqs(chunk.in, in)) =>
      (Some(chunk), heap)
    case chunk :: heap =>
      val (res, rest) = access(pred, in, heap, test)
      (res, chunk :: rest)
  }
}