package underflow.heap

import underflow.error
import underflow.pure.Fun
import underflow.pure.Infix
import underflow.pure.Nilfix
import underflow.pure.Postfix
import underflow.pure.Formfix
import underflow.pure.Non
import underflow.pure.Left
import underflow.pure.Pure
import underflow.pure.Sort
import underflow.pure.App

object Offset {
  /* need to escape underscores that might appear in field names when
     generating the formfix */
  def escape_for_form(str: String): String = {
    val res = new StringBuilder
    for (c <- str) {
      if (c == '_') {
        res.append('\\')
        res.append(c)
      } else {
        res.append(c)
      }
    }
    res.toString
  }

  def arrow(ptr: Pure, field: String, res: Sort): Fun = ptr.typ match {
    case typ @ Sort.pointer(Sort.base(struct)) =>
      Fun("@" + struct + "." + field, List(typ), Sort.pointer(res),Nilfix,Some(("&_->"+escape_for_form(field),Formfix)))
    case _ =>
      throw error.InvalidProgram("invalid field access", ptr, field, res)
  }

  def dot(base: Pure, field: String, res: Sort): Fun = base.typ match {
    case typ @ Sort.base(struct) =>
      val blah = Some(("."+field,Postfix(0)))
      Fun("!" + struct + "." + field, List(typ), res, Nilfix, Some(("."+field,Postfix(0))))
    case _ =>
      throw error.InvalidProgram("invalid field access", base, field, res)
  }

  def index(ptr: Pure): Fun = ptr.typ match {
    case typ @ Sort.pointer(elem) =>
      // Fun("@" + elem, List(typ, Sort.int), typ)
      Fun("+", List(typ, Sort.int), typ, Infix(Left, 7))
    case _ =>
      throw error.InvalidProgram("invalid pointer index", ptr)
  }

  def unapply(pure: Pure) = pure match {
    case App(Fun("+", List(typ @ Sort.pointer(elem), Sort.int), res, _, _), List(arg1, arg2)) =>
      assert(typ == res)
      Some((arg1, arg2))
    case _ =>
      None
  }
}