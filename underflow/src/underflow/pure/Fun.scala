package underflow.pure

import scala.language.postfixOps

case class Fun(name: String, args: List[Sort], ret: Sort, fixity: Fixity = Nilfix, cnamefixity: Option[(String,Fixity)] = None) extends Parametric[Fun] {
  def params = Set((ret :: args) flatMap (_.free): _*)
  def rename(re: TRen) = Fun(name, args map (_ rename re), ret rename re, fixity, cnamefixity)
  def subst(ty: Typing) = Fun(name, args map (_ subst ty), ret subst ty, fixity, cnamefixity)

  def apply(args: Pure*) = App(this, args.toList)
  override def toString = name

  def instance = Fun(name, args map (_.instance), ret.instance, fixity)
  def toStringTyped = name + ": " + args.mkString(", ") + " -> " + ret

  // TODO: add parens only when necessary
  def format(args: List[Any], prec: Int, assoc: Assoc): String = (fixity, args) match {
    case (Nilfix, Nil) =>
      toString
    // case (_, List(arg1, arg2)) if this == Pred._eq(Sort.bool) =>
    //   "(" + arg1 + " <=> " + arg2 + ")"
    //    case (_, List(Eq(arg1, arg2))) =>
    //      "(" + arg1 + " != " + arg2 + ")"
    case (Formfix, args) =>
      "(" + (name form args) + ")"
    case (_: Prefix, List(arg)) =>
      this + " " + arg
    case (_: Postfix, List(arg)) =>
      arg + " " + this
    case (_: Infix, List(arg1, arg2)) =>
      "(" + arg1 + " " + this + " " + arg2 + ")"
    case _ =>
      this + args.mkString("(", ", ", ")")
  }

  def cString(a: Any): String = {
    try {
     val p = a.asInstanceOf[Pure]
     p.cString
    } catch {
      case e:Throwable =>
        a toString
    }
  }
  
  def cformat(args: List[Any], prec: Int, assoc: Assoc): String = {
    val (cname, cfixity) = 
      cnamefixity match { 
        case Some(cnf) =>
          cnf
        case None => (name, fixity)
      }
    (cfixity, args) match {
      case (Nilfix, Nil) =>
        cname
      case (Formfix, args) =>
        "(" + (cname form (args map cString)) + ")"
      case (_: Prefix, List(arg)) =>
        cname+cString(arg)
      case (_: Postfix, List(arg)) =>
        cString(arg)+cname
      case (_: Infix, List(arg1, arg2)) =>
        "(" + cString(arg1) + " " + cname + " " + cString(arg2) + ")"
      case _ =>
        cname + args.map(cString).mkString("(", ", ", ")")
    }
  }
}

object Fun {
  val ite = Fun("_?_:_", List(Sort.bool, Param.alpha, Param.alpha), Param.alpha, Formfix)

  def _true = Fun("true", List(), Sort.bool)
  def _false = Fun("false", List(), Sort.bool)

  def attacker = Fun("attacker", List(), Sort.sec)
  def low = Fun("low", List(), Sort.sec)
  def high = Fun("high", List(), Sort.sec)

  val _null = Fun("null", List(), Sort.pointer(Param.alpha))
  val tid = Fun("tid", List(), Sort.int)

  val lower = Fun("⊑", List(Sort.sec, Sort.sec), Sort.bool, Infix(Non, 6))
  val haslabel = Fun("::", List(Param.alpha, Sort.sec), Sort.bool, Infix(Non, 5))
  val insec = Fun(":!:", List(Param.alpha, Sort.sec), Sort.bool, Infix(Non, 5))
  val insecstrong = Fun(":!!:", List(Param.alpha, Sort.sec), Sort.bool, Infix(Non, 5))  

  val exp = Fun("^", List(Param.alpha, Param.alpha), Param.alpha, Infix(Left, 9))
  val times = Fun("*", List(Param.alpha, Param.alpha), Param.alpha, Infix(Left, 8))
  val divBy = Fun("/", List(Param.alpha, Param.alpha), Param.alpha, Infix(Non, 8))
  val mod = Fun("%", List(Param.alpha, Param.alpha), Param.alpha, Infix(Non, 8))

  val uminus = Fun("-", List(Param.alpha), Param.alpha, Prefix(8))
  val plus = Fun("+", List(Param.alpha, Param.alpha), Param.alpha, Infix(Left, 7))
  val minus = Fun("-", List(Param.alpha, Param.alpha), Param.alpha, Infix(Left, 7))
  // XXX: precedence for the following?
  val bitwise_and = Fun("&", List(Param.alpha, Param.alpha), Param.alpha, Infix(Left, 7))
  val bitwise_or = Fun("|", List(Param.alpha, Param.alpha), Param.alpha, Infix(Left, 7))
  val bitwise_xor = Fun("^", List(Param.alpha, Param.alpha), Param.alpha, Infix(Left, 7))
  val bitwise_neg = Fun("~", List(Param.alpha), Param.alpha, Infix(Left, 7))  
  val shiftleft = Fun("<<", List(Param.alpha, Param.alpha), Param.alpha, Infix(Left, 7))
  val shiftright = Fun(">>", List(Param.alpha, Param.alpha), Param.alpha, Infix(Left, 7))
  
  val _eq = Fun("==", List(Param.alpha, Param.alpha), Sort.bool, Infix(Non, 6))
  val le = Fun("<=", List(Param.alpha, Param.alpha), Sort.bool, Infix(Non, 6))
  val lt = Fun("<", List(Param.alpha, Param.alpha), Sort.bool, Infix(Non, 6))
  val ge = Fun(">=", List(Param.alpha, Param.alpha), Sort.bool, Infix(Non, 6))
  val gt = Fun(">", List(Param.alpha, Param.alpha), Sort.bool, Infix(Non, 6))

  val not = Fun("!", List(Sort.bool), Sort.bool, Prefix(5))
  val and = Fun("&&", List(Sort.bool, Sort.bool), Sort.bool, Infix(Left, 4))
  val or = Fun("||", List(Sort.bool, Sort.bool), Sort.bool, Infix(Left, 3))
  val imp = Fun("==>", List(Sort.bool, Sort.bool), Sort.bool, Infix(Right, 2))
  val eqv = Fun("<=>", List(Sort.bool, Sort.bool), Sort.bool, Infix(Non, 1))

  val nil = Fun("nil", List(), Param.list)
  val cons = Fun("cons", List(Param.alpha, Param.list), Param.list)
  val in = Fun("in", List(Param.alpha, Param.list), Sort.bool)
  val head = Fun("head", List(Param.list), Param.alpha)
  val tail = Fun("tail", List(Param.list), Param.list)
  val last = Fun("last", List(Param.list), Param.alpha)
  val init = Fun("init", List(Param.list), Param.list)

  val select = Fun("_[_]", List(Param.array, Param.alpha), Param.beta, Formfix)
  val store = Fun("_[_:=_]", List(Param.array, Param.alpha, Param.beta), Param.array, Formfix)
}

