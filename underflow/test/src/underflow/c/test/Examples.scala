package underflow.c.test

import minitest.SimpleTestSuite
import underflow.Underflow
import underflow.c
import underflow.error

object Examples extends SimpleTestSuite {
  for (file <- tests) {
    test(file) {
      try {
        c.Verify.interactive = false // get exceptions
        c.verify(file)
      } catch {
        case e: error.Error =>
          println(e.info)
          throw e
      }
    }
  }
  for (file <- discovers) {
    test(file) {
      try {
        c.Verify.interactive = false // get exceptions
        c.Exec.loop = 1 // 1 iteration by default
        c.log.analysisMode = c.AnalysisMode.discover        
        c.verify(file)
      } catch {
        case e: error.Error =>
          println(e.info)
          throw e
      }
    }
  }
  

  def main(args: Array[String]) {
  }
}