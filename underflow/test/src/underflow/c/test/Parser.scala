package underflow.c.test

import minitest.SimpleTestSuite
import underflow.Underflow
import underflow.c
import underflow.error

object Parser extends SimpleTestSuite {
  for (file <- tests) {
    test(file) {
      try {
        c.parse(file); ()
      } catch {
        case e: error.Error =>
          println(e.info)
          throw e
      }
    }
  }

  def main(args: Array[String]) {

  }
}