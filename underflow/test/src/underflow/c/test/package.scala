package underflow.c

package object test {
  val tests = List(
    "examples/insec.c",
    "examples/unreachable.c"
  )
  val discovers = List(
    "examples/Hacl_Policies.c",
    "examples/auction.c",
    "examples/break_continue.c",
    "examples/ct_openssl_utility.c",
    "examples/ct_select.c",
    "examples/ct_sort.c",
    "examples/ct_ssl3_cbc_remove_padding.c",
    "examples/cttk_inner.c",
    "examples/db.c",
    "examples/insec.c",
    "examples/kremlib_base.c",
    "examples/libsodium_binhex.c",
    "examples/libsodium_memcmp.c",
    "examples/location-server.c",
    "examples/pb.c",
    "examples/stack_addr.c",
    "examples/tea.c",
    "examples/tls1_cbc_remove_padding_lucky13.c",
    "examples/tls1_cbc_remove_padding_patch.c",
    "examples/unreachable.c",
    "examples/words_casts.c"
  )
  
}
